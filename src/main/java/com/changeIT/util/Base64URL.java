package com.changeIT.util;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;

public class Base64URL {

	public static String encodeURL(String URL){		
		return Base64.encodeBase64URLSafeString(URL.getBytes());		
	}
	
	public static String decodeURL(String URL){
		return new String(Base64.decodeBase64(URL), StandardCharsets.UTF_8);
	}
	
	 public static void main(String[] args) {
		 String encoded=Base64URL.encodeURL("http://www.google.com/error/");
		 String decoded=Base64URL.decodeURL(encoded);
		 System.out.println(encoded);
		 System.out.println(decoded);
	 }
}
