package com.changeIT.util;

import com.scortelemed.etuw.utils.Encryptor;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Util {
	
	static final private String ALPHABET = "abcdefghijklmnopqrstvwxyz0123456789";
	static final private String ALPHABET2 = "abcdefghijklmnopqrstvwxyz0123456789";
	final private Random rng = new SecureRandom();    

	char randomChar(){
	    return ALPHABET.charAt(rng.nextInt(ALPHABET.length()));
	}
	char randomChar2(){
		return ALPHABET2.charAt(rng.nextInt(ALPHABET2.length()));
	}

	String dossierNumber(int length){
	    
		StringBuilder sb = new StringBuilder();
	    
		int spacer = 0;
	    
		while(length > 0){
	        
	        length--;
	        
	        sb.append(randomChar());
	    }
		
	    return sb.toString();
	}
	String encrypt (String value, String key) {

		return Encryptor.encrypt(key, value);
	}

	String decrypt (String value, String key) {

		return Encryptor.decrypt(key, Base64URL.decodeURL(value));
	}

	String codPedido(){

		StringBuilder sb = new StringBuilder();
		int length = 0;
		for (int i = 0; i < 3; i++) {
			while (length < 4) {

				length++;

				sb.append(randomChar2());
			}
			if (i != 2) {
				sb.append("-");
			}
			length = 0;

		}

		return sb.toString();
		}

	public static Map<String, String> getParameters(String[] params) {
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String[] parameterSplit = param.split("=");
			if (parameterSplit.length > 1) {
				String name = parameterSplit[0];
				String value = parameterSplit[1];
				map.put(name, value);
			} else {
				return null;
			}
		}
		return map;

	}

	static byte[] loadDoc(String fichero) throws IOException {
		FileInputStream fis = new FileInputStream(fichero);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] data = new byte[1024];
		while (fis.read(data) > -1) {
			baos.write(data, 0, data.length);
		}
		fis.close();
		return baos.toByteArray();
	}

	public String getFile (String jasperPath){

		BufferedInputStream stream = new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream(jasperPath));

		byte[] contents = new byte[1024];

		int bytesRead = 0;
		String strFileContents = "";

		try {

			while ((bytesRead = stream.read(contents)) != -1) {
				strFileContents += new String(contents, 0, bytesRead);
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return strFileContents;

	}

	public String fromDateToString_ddMMyyyy (Date date) {

		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String formatedDate = formatter.format(date);
		return formatedDate;

	}
	
}
