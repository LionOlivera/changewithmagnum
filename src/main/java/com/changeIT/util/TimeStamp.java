package com.changeIT.util;


public class TimeStamp {


	public static boolean compareDates(String serverTime) {

		long max = 15;
		long min = 0;
		if (serverTime.isEmpty()) {
			return false;
		} else {
			long diff =System.currentTimeMillis()- Long.valueOf(serverTime);
			long diffMinutesTotales = diff / (60 * 1000);

			if (diff <= 0) {
				return false;
			}
			if (diffMinutesTotales <= max && diffMinutesTotales >= min) {
				return true;
			} else {
				return false;
			}

		}

	}

}