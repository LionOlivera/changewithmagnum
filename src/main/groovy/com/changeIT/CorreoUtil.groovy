package com.changeIT

import groovy.util.logging.Slf4j

import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

@Slf4j
class CorreoUtil {

    String sendMail(String message, String subject, String to) {

        boolean debug = false
        Session sessionMail = Session.getDefaultInstance(propertiesMail())


        try {
            sessionMail.setDebug(debug)
            Message msg = new MimeMessage(sessionMail)
            InternetAddress addressFrom = new InternetAddress(Configuration.findByProperty("mail.remitente").value)
            msg.setFrom(addressFrom)

            if (to) {

                InternetAddress[] addressTo = new InternetAddress(to)

                msg.setRecipients(Message.RecipientType.TO, addressTo)
                msg.setSubject(subject)
                msg.setContent(message, "text/html; charset=utf-8")
                Transport t = sessionMail.getTransport("smtp")
                if (Configuration.findByProperty("mailConfig").value.equalsIgnoreCase("clienteX")) {
                    t.connect()
                } else if (Configuration.findByProperty("mailConfig").value.equalsIgnoreCase("gmail") || Configuration.findByProperty("mailConfig").value.equalsIgnoreCase("scor")) {
                    t.connect(Configuration.findByProperty("mail.usuarioConexion").value, Configuration.findByProperty("mail.passConexion").value)
                }
                t.sendMessage(msg, msg.getAllRecipients())
                t.close()
                log.info("Email enviado sactifactoriamente a ${to}")
                return true
            }
        } catch (Exception e) {
            log.error("No se ha podido en enviar el email", e)
            return false
        }
    }

    private Properties propertiesMail() {

        Properties props = new Properties()
        if (Configuration.findByProperty("mailConfig").value.equalsIgnoreCase("gmail")) {
            props.put("mail.smtp.host", Configuration.findByProperty("mail.smtp.host").value)
            props.put("mail.smtp.auth", Configuration.findByProperty("mail.smtp.auth").value)
            props.put("mail.debug", Configuration.findByProperty("mail.debug").value)
            props.put("mail.smtp.port", Configuration.findByProperty("mail.smtp.port").value)
            props.put("mail.smtp.socketFactory.port", Configuration.findByProperty("mail.smtp.socketFactory.port").value)
            props.put("mail.smtp.socketFactory.fallback", Configuration.findByProperty("mail.smtp.socketFactory.fallback").value)
            props.put("mail.smtp.socketFactory.class", Configuration.findByProperty("mail.smtp.socketFactory.class").value)
//            props.put("mail.smtp.ssl.trust", Configuration.findByProperty("mail.smtp.ssl.trust").value)
        } else if (Configuration.findByProperty("mailConfig").value.equalsIgnoreCase("clienteX")) {

        } else {
//            agregar log - envio a  por defecto.

        }

        return props
    }
}
