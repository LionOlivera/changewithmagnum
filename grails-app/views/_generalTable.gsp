<div class="product_list">
    <%@ page import="com.changeIT.Orders" %>
    <div class="container-fluid">
        <div class="row">
            <g:each in="${productList}" status="i" var="productInstance">
                <article class="col-md-3">
                    <div class="product_image">
                        <g:if test="${productInstance.featuredImageBytes}">
                            <img src="<g:createLink controller="product" action="featuredImage" id="${productInstance.id}"/>" width="400"/>
                        </g:if>
                    </div>
                    <div class="product_text">
                        <h2>${productInstance?.articulo}</h2>
                        <div class="row">
                            <div class="col-md-8"><p>${productInstance?.obs}</p></div>
                            <div class="col-md-4"><p class="precio">$ ${productInstance?.precio}</p></div>
                        </div>
                    </div>
                    <div class="product_action">
                        <select name="" id="">
                            <option value="1">1 ud.</option>
                            <option value="2">2 ud.</option>
                            <option value="3">3 ud.</option>
                            <option value="4">4 ud.</option>
                        </select>
                        <g:link controller="orders" action="create"  class="btn add_button " id="${productInstance?.id}">Añadir</g:link>
                    </div>
                </article>
            </g:each>
        </div>
    </div>
</div>


