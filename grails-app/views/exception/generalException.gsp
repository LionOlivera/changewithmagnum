<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Change IT</title>
    <asset:stylesheet src="style.css" />
    <asset:stylesheet src="main.css" />

</head>
<body>

<div class="container">
    <div class="row">


        <!-- ngIf: !configurator.isLoading && (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword') -->
        <div class="col-md-8 offset-md-2 no-padding ng-scope " style="margin-top: 20px;"
             ng-if="!configurator.isLoading &amp;&amp; (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword')">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="ul-title ul_large mainTitle ng-binding"
                        ng-bind-html="configurator.screenTitle">
                        <g:message code="error.message.title"/>
                    </h4>
                </div>

            <!-- ngIf: !configurator.error -->
            <div class="ul-paper mainMargin ng-scope" ng-if="!configurator.error">
                <!-- ngIf: configurator.isLoading -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'welcome' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->
                <div class="panel-body ng-scope"
                     ng-if="!configurator.isLoading &amp;&amp; configurator.step === 'changePassword'">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center error" ><g:message code="error.message.general"/></h2>
                            <div class="row">
                                %{--<p class="col-md-6">--}%
                                    %{--${message}--}%
                                %{--</p>--}%
                                <div class=" col-md-4 offset-md-4">
                                <g:link controller="${cont}" action="${act}" class="btn btn-default btn-error"><g:message code="error.message.btn"/></g:link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'reviewDatas' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'velogicaCGU' -->
            </div>
            <!-- end ngIf: !configurator.error -->
            <!-- ngIf: configurator.error -->
        </div>
    </div>
    </div>
</div>
</body>
</html>