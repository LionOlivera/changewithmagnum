<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Scor.</title>
    <asset:stylesheet src="style.css" />
    <asset:stylesheet src="main.css" />

</head>
<body>

<div class="container">
    <div class="row">


        <!-- ngIf: !configurator.isLoading && (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword') -->
        <div class="col-md-8 offset-md-2 no-padding ng-scope panel panel-default" style="margin-top: 20px;"
             ng-if="!configurator.isLoading &amp;&amp; (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword')">
            <div class="ul-paper ul_invert panel-heading">
                <div class="ul-paper-head">
                    <h4 class="ul-title ul_large mainTitle ng-binding"
                        ng-bind-html="configurator.screenTitle">
                        Bienvenue sur votre espace personnel sécurisé de sélection médicale<br>Scor.
                    </h4>
                </div>
            </div>
            <!-- ngIf: !configurator.error -->
            <div class="ul-paper mainMargin ng-scope" ng-if="!configurator.error">
                <!-- ngIf: configurator.isLoading -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'welcome' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->
                <div class="panel-body ng-scope"
                     ng-if="!configurator.isLoading &amp;&amp; configurator.step === 'changePassword'">
                    <div class="row" id="changePassword">
                        <div class="col-md-12">
                            <h3 class="ul-title ul_medium">SE HA GENERADO UNA EXCEPCION</h3>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 password-hint text-center ng-binding">"${message}"
                                </div>
                                "${link}"
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'reviewDatas' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'velogicaCGU' -->
            </div>
            <!-- end ngIf: !configurator.error -->
            <!-- ngIf: configurator.error -->
        </div>
    </div>
</div>
</body>
</html>