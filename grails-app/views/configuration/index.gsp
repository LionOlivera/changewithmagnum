<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'configuration.label', default: 'Configuration')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-configuration" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                    default="Skip to content&hellip;"/></a>
<g:render template="/layouts/nav"/>
<div id="list-configuration" class="content scaffold-list container-fluid" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <sec:ifAnyGranted roles="ROLE_ADMIN">
        <g:link class="btn create" action="create"><i class="fas fa-plus-circle"></i> <g:message code="default.new.label"
                                                              args="[entityName]"/></g:link>
    </sec:ifAnyGranted>
    <f:table collection="${configurationList}"/>

    <div class="pagination">
        <g:paginate total="${configurationCount ?: 0}"/>
    </div>
</div>
</body>
</html>