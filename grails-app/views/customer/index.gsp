<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'customer.label', default: 'Customer')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>
<a href="#list-customer" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<g:render template="/layouts/nav"/>

<div id="list-customer" class="content container-fluid scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
        <g:link class="create btn" action="create"><i class="fas fa-plus-circle"></i> <g:message code="default.new.label"
                                                              args="[entityName]"/></g:link>
    </sec:ifAnyGranted>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>


    <div class="">
        <div class="row opciones">
            <br><br><br>
        </div>


        <div class="card">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Adress</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${customerList}" status="i" var="customerInstance">
                        <tr>
                            <td>
                                ${fieldValue(bean: customerInstance, field: "name")}
                            </td>
                            <td>
                                ${fieldValue(bean: customerInstance, field: "lastName")}
                            </td>
                            <td>
                                <g:link action="show"
                                        id="${customerInstance.id}">${fieldValue(bean: customerInstance, field: "email")}</g:link>
                            </td>
                            <td>
                                ${fieldValue(bean: customerInstance, field: "address")}
                            </td>
                            <td>
                                <g:link action="indexCustomer" controller="orders"
                                        id="${customerInstance.id}">Pedidos</g:link>
                            </td>
                        </tr>
                    </g:each>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <g:if test="${customerCount > 0}">
        <div class="pagination">
            <g:paginate total="${customerCount ?: 0}"/>
        </div>
    </g:if>
</div>
</body>
</html>