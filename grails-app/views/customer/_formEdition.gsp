<%@ page import="com.changeIT.Customer" %>
<div class="col-md-8 offset-md-2">
    <div
            class="fieldcontain row ${hasErrors(bean: customer, field: 'email', 'error')} required">
        <label for="email" class="col-md-6"><g:message code="customer.form.email" default="Email"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="email" class="col-md-6" required="" value="${customer?.email}"/>
    </div>

    <div
            class="fieldcontain row  ${hasErrors(bean: customer, field: 'name', 'error')} required">
        <label for="name" class="col-md-6"><g:message
                code="customer.form.name" default="Nom"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="name" class="col-md-6" required=""
                     value="${customer?.name}"/>
    </div>

    <div
            class="fieldcontain row ${hasErrors(bean: customer, field: 'lastName', 'error')} required">
        <label for="lastName" class="col-md-6"><g:message
                code="customer.form.lastname" default="Prenom"/> <span
                class="required-indicator">*</span>
        </label>
        <g:textField name="lastName" class="col-md-6" required=""
                     value="${customer?.lastName}"/>
    </div>
    <div    class="fieldcontain row ${hasErrors(bean: customer, field: 'gender', 'error')} required">
        <label for="lastName" class="col-md-6"><g:message
                code="customer.form.gender" default="Genero"/> <span
                class="required-indicator">*</span>
        </label>
        <g:select name="gender"
                  value="${customer?.gender}"
                  from="[message(code: 'customerSexMale'), message(code: 'customerSexFemale')]" noSelection="['customerSexMale': message(code: 'customer.sex.gender')]"
                  optionKey=""
                  class="col-md-6"/>
    </div>

    <div
            class="fieldcontain row ${hasErrors(bean: customer, field: 'address', 'error')} required">
        <label for="address" class="col-md-6"><g:message
                code="customer.form.address"
                default="Adresse/ Code postal/ Ville"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textArea class="col-md-6" name="address" required=""
                    value="${customer?.address}"/>
    </div>


    <div class="fieldcontain row ${hasErrors(bean: customer, field: 'cuit', 'error')} required">
        <label for="address" class="col-md-6"><g:message
                code="customer.form.cuit"
                default="cuit"/> <span
                class="required-indicator">*</span>
        </label>
        <g:textField class="col-md-6" name="cuit" required=""
                     value="${customer?.cuit}"/>
    </div>

    <div class="fieldcontain row ${hasErrors(bean: customer, field: 'razonSocial', 'error')} required">
        <label for="address" class="col-md-6"><g:message
                code="customer.form.razonSocial"
                default="razonSocial"/> <span
                class="required-indicator">*</span>
        </label>
        <g:textField class="col-md-6" name="razonSocial" required=""
                     value="${customer?.razonSocial}"/>
    </div>

    <div class="fieldcontain row ${hasErrors(bean: customer, field: 'phone', 'error')} required">
        <label for="address" class="col-md-6"><g:message
                code="customer.form.phone"
                default="Telefono"/> <span
                class="required-indicator">*</span>
        </label>
        <g:textField class="col-md-6" name="phone" required=""
                     value="${customer?.phone}"/>
    </div>
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
        <div class="fieldcontain row ${hasErrors(bean: customer, field: 'limiteCompra', 'error')} required">
            <label for="address" class="col-md-6"><g:message
                    code="customer.form.limiteCompra"
                    default="limiteCompra"/> <span
                    class="required-indicator">*</span>
            </label>
            <g:textField class="col-md-6" name="limiteCompra" required=""
                         value="${customer?.limiteCompra}"/>
        </div>
    </sec:ifAnyGranted>

</div>