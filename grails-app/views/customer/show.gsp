<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'customer.label', default: 'Customer')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-customer" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                               default="Skip to content&hellip;"/></a>

<g:render template="/layouts/nav"/>

<div id="show-customer" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
%{-- <f:display bean="customer"/>--}%

    <ol class="property-list dossier">
        <g:if test="${customer?.name}">
            <li class="fieldcontain row"><span id="name-label" class="property-label col-md-6"><g:message
                    code="customer.name.label" default="Name"/></span> <span
                    class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                field="name"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.lastName}">
            <li class="fieldcontain row"><span id="lastName-label" class="property-label col-md-6"><g:message
                    code="customer.lastName.label" default="Last Name"/></span> <span
                    class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                field="lastName"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.email}">
            <li class="fieldcontain row"><span id="email-label" class="property-label col-md-6"><g:message
                    code="customer.email.label" default="Email"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="email"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.gender}">
            <li class="fieldcontain row"><span id="gender-label"
                                               class="property-label col-md-6"><g:message
                        code="dossier.gender.label" default="Genre"/></span> <span
                    class="property-value col-md-6" aria-labelledby="gender-label"><g:message
                        code="${customer?.gender}"/></span></li>
        </g:if>
        <g:if test="${customer?.address}">
            <li class="fieldcontain row"><span id="address-label" class="property-label col-md-6"><g:message
                    code="customer.address.label" default="Address"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="address"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.cuit}">
            <li class="fieldcontain row"><span id="cuit-label" class="property-label col-md-6"><g:message
                    code="customer.cuit.label" default="Cuit"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="cuit"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.razonSocial}">
            <li class="fieldcontain row"><span id="razonSocial-label" class="property-label col-md-6"><g:message
                    code="customer.razonSocial.label" default="Razon Cocial"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="cuit"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.phone}">
            <li class="fieldcontain row"><span id="limiteCompra-label" class="property-label col-md-6"><g:message
                    code="customer.limiteCompra.label" default="Limite de Compra"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="limiteCompra"/></span>
            </li>
        </g:if>
        <g:if test="${customer?.limiteCompra}">
            <li class="fieldcontain row"><span id="limiteCompra-label" class="property-label col-md-6"><g:message
                    code="customer.phone.label" default="Telefono"/></span>
                <span class="property-value col-md-6" aria-labelledby="email-label"><g:fieldValue bean="${customer}"
                                                                                                  field="phone"/></span>
            </li>
        </g:if>


        <g:form resource="${this.customer}" method="DELETE">
            <fieldset class="buttons">
                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR,ROLE_CLIENTE ">
                    <g:link class="edit" action="edit" resource="${this.customer}"><g:message
                            code="default.button.edit.label" default="Edit"/></g:link>
                </sec:ifAnyGranted>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <input class="delete" type="submit"
                           value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                           onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

                </sec:ifAnyGranted>
            </fieldset>
        </g:form>
</div>
</body>
</html>
