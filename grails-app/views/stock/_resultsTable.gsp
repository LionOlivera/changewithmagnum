<g:form useToken="${true}" action="save">
    <fieldset class="form">
    <%@ page import="com.changeIT.Orders" %>
    <div id="flotante">

        <div class=" ${hasErrors(bean: orders, field: 'cantidad', 'error')} required">
            <table class="table">
                <col style="width:10%">
                <col style="width:40%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <thead>
                <tr>
                    <th data-hql-path="stock.id" onclick="onClickTableHeader(this)">Codigo<i></i></th>
                    <th data-hql-path="stock.articulo" onclick="onClickTableHeader(this)">Articulo<i></i></th>
                    <th data-hql-path="stock.precio" onclick="onClickTableHeader(this)">Precio<i></i></th>
                    <th data-hql-path="stock.cantidad" onclick="onClickTableHeader(this)">Cantidad<i></i></th>
                    <th data-hql-path="stock.mdl" onclick="onClickTableHeader(this)">Mdl<i></i></th>
                    <th data-hql-path="stock.tipo" onclick="onClickTableHeader(this)">Tipo<i></i></th>
                    <th data-hql-path="stock.obs" onclick="onClickTableHeader(this)">Obs<i></i></th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${stockInstanceList}" status="i" var="stockInstance">
                    <tr>
                        <td><g:link controller="stock" action="show"     id="${stockInstance?.id}">${fieldValue(bean: stockInstance, field: "id")}</g:link></td>
                        <td><g:textField name="articulo" required="" value="${stockInstance?.product?.articulo}"/></td>
                        <td><g:textField name="precio" required="" value="${stockInstance?.product?.precio}"/></td>
                        <td><g:textField name="cantidad" required="" value="${stockInstance?.cantidad}"/></td>
                        <td><g:textField name="mdl" required="" value="${stockInstance?.product?.mdl}"/></td>
                        <td><g:textField name="tipo" required="" value="${stockInstance?.product?.tipo}"/></td>
                        <td><g:textField name="obs" required="" value="${stockInstance?.obs}"/></td>
                        <g:hiddenField name="stockInstance" value="${stockInstance.product?.id}"/>
                    </tr>
                </g:each>

                </tbody>
            </table>

        </div>

    </div>
    </div>

</fieldset>
    <g:hiddenField name="total" value="${stockInstanceTotal}"/>
    <g:hiddenField name="offset" value="${offset}"/>
    <g:hiddenField name="orderBy" value="${orderBy}"/>
    <g:hiddenField name="orderMode" value="${orderMode}"/>
</g:form>