<%@ page import="com.changeIT.Customer" %>
<div class="col-md-8 offset-md-2">
    <div    class="fieldcontain row ${hasErrors(bean: stock?.product, field: 'articulo', 'error')} required">
        <label for="email" class="col-md-6"><g:message code="stock.form.articulo" default="Email"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="articulo" class="col-md-6" required=""disabled="true" value="${stock?.product.articulo}"/>
    </div>

    <div  class="fieldcontain row  ${hasErrors(bean: stock, field: 'cantidad', 'error')} required">
        <label for="cantidad" class="col-md-6"><g:message
                code="stock.form.cantidad" default="Nom"/>
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="cantidad" class="col-md-6" required="true"  value="${stock?.cantidad}"/>
    </div>

    <div   class="fieldcontain row ${hasErrors(bean: stock, field: 'obs', 'error')} required">
        <label for="obs" class="col-md-6"><g:message
                code="stock.form.obs" default="Prenom"/> <span
                class="required-indicator">*</span>
        </label>
        <g:textField name="obs" class="col-md-6" required="true"  value="${stock?.obs}"/>
    </div>







</div>