<div class="nav" role="navigation">
    <ul class="menu">


        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link controller="orders" action="index">Pedidos</g:link></li>

        <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">

            <li><g:link controller="stock" action="index">Stock</g:link></li>
            <li><g:link controller="product" action="index">Administracion de Productos</g:link></li>
        </sec:ifAnyGranted>
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li><g:link controller="secUser" action="index">Usuarios</g:link></li>
            <li><g:link controller="state" action="index">Estados</g:link></li>
            <li><g:link controller="excelImporter" action="index">Importar Stock</g:link></li>
            <li><g:link controller="configuration" action="index">Parameters</g:link></li>
        </sec:ifAnyGranted>


        <g:render template="/orders/placeOrder"/>

    </ul>

</div>