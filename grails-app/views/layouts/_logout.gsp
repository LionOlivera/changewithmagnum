<g:form useToken="${true}" controller="logout" action="index">
    <button type="submit" class="btn-link">
        <i class="fas fa-sign-out-alt"></i> <g:message code="logout"/>  </button>
</g:form>