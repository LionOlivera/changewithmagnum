<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <asset:stylesheet src="style.css"/>

    <link rel="icon" href="img/favicon.png">

    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>
<body>


<div class="login-bg">
    <div class="container-fluid">
        <div class="row pt-4">
            <div class="col-md-6 offset-md-3 text-center">
                <asset:image src="logo2.png" alt="Mayorista Magnum" class="img-responsive"/>

                <div class="login-box">
                    <h4>Please Login</h4>
                    <form action='${postUrl}' method='POST' id='loginForm'>
                        <div class="form-group row">
                            <label for="user" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                                <input
                                        type="text" name='username' id='username' class="form-control"
                                        placeholder="User" required autofocus
                                        class="form-control ng-pristine ng-untouched ng-empty ng-valid-email ng-invalid ng-invalid-required"
                                        ng-attr-type="email" ng-model="login.email"
                                        ng-attr-placeholder="{{ 'LOGIN.emailInfo' | translate }}"
                                        ng-required="true" name="email"
                                        placeholder="Entrez votre email" required="required"
                                        type="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pass" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name='password' id='password' class="form-control"
                                       placeholder="Password" required
                                       class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required"
                                       ng-attr-type="password" ng-model="login.password;"
                                       placeholder="Entrez votre mot de passe" ng-required="true"
                                       ng-keypress="login.checkEnter($event)" name="password"
                                       required="required" type="password">
                            </div>
                        </div>





                        <button type="submit" name="button" class="btn">Login</button>
                        <br><br>
                        <div class="">
                            <a data-toggle="modal" data-target="#modal"  href=""><g:message code="login.password.recovery.message.0"/></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




<footer>
    <div class="row">
        <div class="col-md-12 text-center">
            <p>Mayorista Magnum todos los derechos reservados. Copyright 2019</p>
        </div>
    </div>
</footer>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
    <div class="modal-dialog" style="max-width: 500px !important;" role="document">

        <div class="modal-content">
            <div class="modal-header">
                %{--<h5 class="modal-title" id="modalLabel">Email Sent</h5>--}%
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="col-md-12">
                    <h4><i class="fas fa-info-circle"></i><g:message code="login.password.recovery.message.4"/></h4>
                </div>
                <br>
                <g:form useToken="${true}" controller="login" action="retrivePasswordEmail">
                    <fieldset class="form">
                        <g:render template="form"/>
                    </fieldset>
                    <div align="right">
                        <recaptcha:ifEnabled>
                            <recaptcha:recaptcha lang='fr'/>
                        </recaptcha:ifEnabled>
                    </div>
                    <fieldset class="buttons">
                        <g:submitButton name="create" class="btn btn-default" value="${message(code:'login.password.recovery.message.5')}"/>
                    </fieldset>

                </g:form>
            </div>

        </div>

    </div>
</div>
<script>
    document.getElementById("username").addEventListener("keyup", checkFilled);
    document.getElementById("password").addEventListener("keyup", checkFilled);

    function checkFilled() {
        var inputVal = document.getElementById("username");
        var inputValPassword = document.getElementById("password");
        if (inputVal.value == "" || inputValPassword.value == "") {
            document.getElementById('submit').className = 'btn btn-disable ng-binding';
            document.getElementById("submit").disabled = true;
        }
        else {
            document.getElementById('submit').className = 'btn btn-default ng-binding';
            document.getElementById("submit").disabled = false;
        }
    }

</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
