<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="style.css"/>
    <asset:stylesheet src="main.css"/>
</head>

<body>
<div class="container">
    <div class="row">
        <!-- ngIf: !configurator.isLoading && (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword') -->
        <div class="col-md-7  welcome "
             ng-if="!configurator.isLoading &amp;&amp; (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword')">
            <div class="panel panel-default flex registration-header">
                <div class="text-wrap">
                    <h1 class=""
                        ng-bind-html="configurator.screenTitle">
                        <g:message code="registration.registration.header"/>
                        <div class="divider mt-3"></div>
                    </h1>
                </div>
            </div>
        </div>
        <!-- end ngIf: !configurator.isLoading && (configurator.step === 'reviewDatas' || configurator.step === 'welcome' || configurator.step === 'changePassword') -->
        <div class="col-md-5 login-box">
            <!-- ngIf: !configurator.error -->
            <div class="panel panel-default" ng-if="!configurator.error">
                <!-- ngIf: configurator.isLoading -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'welcome' -->
                <!-- ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->

                <div class="panel-heading">
                    <h4 class=""><g:message code="registration.registration.password.message"/></h4>
                    <g:if test="${error}">
                        <ul class="errors" role="alert">
                            <li>
                                <g:message error="${error}"/>
                            </li>
                        </ul>
                    </g:if>
                </div>

                <div class="panel-body ng-scope"
                     ng-if="!configurator.isLoading &amp;&amp; configurator.step === 'changePassword'">
                <div class="" id="changePassword">

                    <g:form useToken="${true}" method="post"
                            class="form-horizontal ng-pristine ng-invalid ng-invalid-password-rule ng-invalid-required ng-valid-minlength ng-valid-password-match"
                            name="configurator.changePasswordForm" novalidate="novalidate">
                        <div class="form-group row">
                            <label
                                    ng-class="{ error: connexionform.email.$touched &amp;&amp; !connexionform.email.$valid }"
                                    uib-tooltip="Entrez votre email" class="ng-binding col-sm-4 col-form-label"
                                    for="inputEmail">Mon
                            <g:message code="registration.registration.password"/>
                            </label>

                            <div class="col-md-8">
                                <input
                                        type="password" name='j_username' id='username' class="form-control"
                                        placeholder="Mot de passe" required autofocus
                                        class="form-control ng-pristine ng-untouched ng-empty ng-valid-email ng-invalid ng-invalid-required"
                                        ng-attr-type="password" ng-model="login.email"
                                        ng-attr-placeholder="{{ 'LOGIN.emailInfo' | translate }}"
                                        ng-required="true" name="email"
                                        placeholder="Entrez votre email" required="required"
                                        type="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword"
                                   ng-class="{ error: connexionform.password.$touched &amp;&amp; !connexionform.password.$valid }"
                                   class="ng-binding col-sm-4 col-form-label"><g:message
                                    code="registration.registration.confirmation"/>
                                <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top"
                                   title="Les deux mots de passe doivent être identiques"></i>
                            </label>

                            <div class="col-md-8">
                                <input type="password" name='j_password' id='password' class="form-control"
                                       placeholder="Retaper le mot de passe" required
                                       class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required"
                                       ng-attr-type="password" ng-model="login.password;"
                                       placeholder="Entrez votre mot de passe" ng-required="true"
                                       ng-keypress="login.checkEnter($event)" name="password"
                                       required="required" type="password">
                            </div>

                        </div>
                        </div>
                        <div class="row">
                            <div
                                    class="col-md-12 password-hint text-center ng-binding">
                                <p>
                                    <g:message code="registration.registration.password.security.message"/>

                                </p></div>

                            <div class="btn btn-default btn2 col-md-4 offset-md-4" id="submit">
                                <g:actionSubmit value="Suivant" class=""          action="updateUser"></g:actionSubmit>
                            </div>

                        </div>
                    </g:form>
                </div>
            </div>
            <!-- end ngIf: !configurator.isLoading && configurator.step === 'changePassword' -->
            <!-- ngIf: !configurator.isLoading && configurator.step === 'reviewDatas' -->
            <!-- ngIf: !configurator.isLoading && configurator.step === 'velogicaCGU' -->
        </div>
        <!-- end ngIf: !configurator.error -->
        <!-- ngIf: configurator.error -->
    </div>
</div>
</div>


</body>
</html>