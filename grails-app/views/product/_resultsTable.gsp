<g:form useToken="${true}" action="save">
    <fieldset class="form">
    <%@ page import="com.changeIT.Orders" %>
    <div id="flotante">

        <div class=" ${hasErrors(bean: orders, field: 'cantidad', 'error')} required">
            <table class="table">
                <col style="width:10%">
                <col style="width:40%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <thead>
                <tr>
                    <th data-hql-path="product.id" onclick="onClickTableHeader(this)">Codigo<i></i></th>
                    <th data-hql-path="product.articulo" onclick="onClickTableHeader(this)">Articulo<i></i></th>
                    <th data-hql-path="product.precio" onclick="onClickTableHeader(this)">Precio<i></i></th>
                    <th data-hql-path="product.tipo" onclick="onClickTableHeader(this)">Tipo<i></i></th>
                    <th data-hql-path="product.mdl" onclick="onClickTableHeader(this)">Mdl<i></i></th>
                    <th data-hql-path="product.obs" onclick="onClickTableHeader(this)">Obs<i></i></th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${productList}" status="i" var="stockInstance">
                    <tr>
                        <td><g:link controller="product" action="show"
                                    id="${stockInstance?.id}">${fieldValue(bean: stockInstance, field: "id")}</g:link></td>
                        <td><g:textField name="articulo" required="" value="${stockInstance?.articulo}"/></td>
                        <td><g:textField name="precio" required="" value="${stockInstance?.precio}"/></td>
                        <td><g:textField name="tipo" required="" value="${stockInstance?.tipo}"/></td>
                        <td><g:textField name="mdl" required="" value="${stockInstance?.mdl}"/></td>

                        <td><g:textField name="obs" required="" value="${stockInstance?.obs}"/></td>
                        <g:hiddenField name="stockInstance" value="${stockInstance?.id}"/>
                    </tr>
                </g:each>

                </tbody>
            </table>

        </div>

    </div>
    </div>

</fieldset>
    <g:hiddenField name="total" value="${productInstanceTotal}"/>
    <g:hiddenField name="offset" value="${offset}"/>
    <g:hiddenField name="orderBy" value="${orderBy}"/>
    <g:hiddenField name="orderMode" value="${orderMode}"/>
</g:form>