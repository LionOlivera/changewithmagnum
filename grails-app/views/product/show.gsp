<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<g:render template="/layouts/nav"/>

<div id="show-product" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <fieldset class="form">
        <g:render template="form"/>
    </fieldset>


    <g:form resource="${this.product}" method="DELETE">
        <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
        <fieldset class="buttons">
            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
                <g:link class="edit" action="edit" resource="${this.product}"><g:message
                        code="default.button.edit.label" default="Edit"/></g:link>
            </sec:ifAnyGranted>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <input class="delete" type="submit"
                       value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </sec:ifAnyGranted>
        </fieldset>
        </sec:ifAnyGranted>
    </g:form>
</div>
</body>
</html>
