<g:uploadForm name="uploadFeaturedImage" action="uploadFeaturedImage">
    <g:hiddenField name="id" value="${this.product?.id}" />
    <g:hiddenField name="version" value="${this.product?.version}" />
    <input type="file" name="featuredImageFile" />
    <fieldset class="buttons">
        <input class="save" type="submit" value="${message(code: 'productList.featuredImage.upload.label', default: 'Upload')}" />
    </fieldset>
</g:uploadForm>