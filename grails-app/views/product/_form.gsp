<%@ page import="com.changeIT.Customer" %>

<div class="row">
    <div class="col-md-3">

        <g:if test="${this.product.featuredImageBytes}">
            <img src="<g:createLink controller="product" action="featuredImage" id="${this.product.id}"/>" width="400"/>
        </g:if>
        <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
            <g:link class="btn edit edit_image" action="editFeaturedImage" resource="${this.product}"><g:message
                    code="product.featuredImageUrl.edit.label" default="Edit Featured Image"/></g:link>

        </sec:ifAnyGranted>
    </div>

    <div class="col-md-9">

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'codigo', 'error')} required">
            <label for="codigo" class="col-md-4 control-label"><g:message
                    code="product.form.codigo" default="Codigo"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="codigo" class="form-control" required="" disabled="true" value="${product?.codigo}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'articulo', 'error')} required">
            <label for="articulo" class="col-md-4 control-label"><g:message code="product.form.articulo"
                                                                            default="articulo"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="created" class="form-control" required="" disabled="true"
                             value="${product?.articulo}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'precio', 'error')} required">
            <label for="precio" class="col-md-4 control-label"><g:message
                    code="product.form.precio" default="Precio"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="precio" class="form-control" required="" disabled="true"
                             value="${product?.precio}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'tipo', 'error')} required">
            <label for="tipo" class="col-md-4 control-label"><g:message
                    code="product.form.tipo" default="Tipo"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="tipo" class="form-control" required="" disabled="true"
                             value="${product?.tipo}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'mdl', 'error')} required">
            <label for="mdl" class="col-md-4 control-label"><g:message
                    code="product.form.mdl" default="mdl"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="mdl" class="form-control" required="" disabled="true"
                             value="${product?.mdl}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: product, field: 'obs', 'error')} required">
            <label for="obs" class="col-md-4 control-label"><g:message
                    code="product.form.obs" default="Obs"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="obs" class="form-control" required="" disabled="true"
                             value="${product?.obs}"/>
            </div>
        </div>

    </div>

</div>



