<%@ page import="com.changeIT.SecUser" %>
<div class="row">
    <div class="col-md-6">
        <div class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'username', 'error')} required">
            <label for="username" class="col-md-4"><g:message code="secuser.form.user" default="User"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="username" required=""
                             value="${secUserInstance?.username}" class="form-control"/>
            </div>
        </div>

        <div class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'password', 'error')} required">
            <label for="password" class="col-md-4"><g:message code="secuser.form.password" default="Password"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:passwordField name="password"  required=""  class="form-control"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'name', 'error')} required">
            <label for="name" class="col-md-4"><g:message code="secuser.form.name" default="Name"/>
            </label>

            <div class="col-md-8">


                <g:textField name="name" required="" value="${secUserInstance?.name}" class="form-control"/>
            </div>
        </div>


        <div
                class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'lastName', 'error')} required">
            <label for="lastName" class="col-md-4"><g:message
                    code="secuser.form.lastName"
                    default="Last Name"/>
            </label>

            <div class="col-md-8">

            <g:textField name="lastName" required=""
                         value="${secUserInstance?.lastName}" class="form-control"/>

            </div>

        </div>

        <div
                class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'email', 'error')} required">
            <label for="email" class="col-md-4"><g:message
                    code="secuser.form.email"
                    default="Email"/>
            </label>
            <div class="col-md-8">
            <g:textField name="email" required=""
                         value="${secUserInstance?.email}" class="form-control"/>
            </div>
        </div>

        <div
                class="fieldcontain row ${hasErrors(bean: secUserInstance, field: 'phone', 'error')} required">
            <label for="phone" class="col-md-4"><g:message
                    code="secuser.form.phone"
                    default="Phone"/>
            </label>
            <div class="col-md-8">
            <g:textField name="phone" required=""
                         value="${secUserInstance?.phone}" class="form-control"/>

            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div
                class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'enabled', 'error')}">
            <label class="check-box-label container" for="accountLocked"><g:message
                    code="secuser.form.enable"
                    default="Enabled"/>
            <g:checkBox name="enabled"
                        value="${secUserInstance?.enabled}"/>
                <span class="checkmark"></span>
            </label>

        </div>

        <div
                class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'accountLocked', 'error')} required">
            <label class="check-box-label container" for="accountLocked"><g:message
                    code="secuser.form.accountLocked"
                    default="Account locked"/>

            <g:checkBox name="accountLocked"
                        value="${secUserInstance?.accountLocked}"/>
                <span class="checkmark"></span>
            </label>
        </div>

        <div
                class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'passwordExpired', 'error')} required">
            <label class="check-box-label container" for="passwordExpired"><g:message
                    code="secuser.form.passwordExpired"
                    default="Password expired"/>
            <g:checkBox name="passwordExpired"
                        value="${secUserInstance?.passwordExpired}"/>
                <span class="checkmark"></span>
            </label>

        </div>

        <div
                class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'accountExpired', 'error')} required">
            <label class="check-box-label container" for="accountExpired"><g:message
                    code="secuser.form.accountExpired"
                    default="Account expired"/>
            <g:checkBox name="accountExpired"
                        value="${secUserInstance?.accountExpired}"/>
                <span class="checkmark"></span>
            </label>

        </div>

    </div>

    <div class="col-md-3">

        <g:each in="${roleMap}" var="role" status="i">
            <div class="fieldcontain form-group">

                <label class="check-box-label container"><g:message
                        code="secUserInstance.role.label"
                        default="${role.key.authority}"/>

                <g:radio name="role" value="${role.key.id}"
                         checked="${role.value}"/>
                    <span class="checkmark radio"></span>

                </label>
            </div>
        </g:each>
    </div>
</div>