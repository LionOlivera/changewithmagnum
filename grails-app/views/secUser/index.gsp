<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'secUser.label', default: 'SecUser')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-secUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>
<g:render template="/layouts/nav"/>
<div id="list-secUser" class="content container-fluid scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <sec:ifAnyGranted roles="ROLE_ADMIN">
        <g:link class="create btn" action="create"><i class="fas fa-plus-circle"></i> <g:message code="default.new.label"
                                                              args="[entityName]"/></g:link>
    </sec:ifAnyGranted>
    <div id="resultsTableDiv" class="card">
        <g:render template="resultsTable"/>
    </div>

    <div class="pagination">
        <g:paginate total="${secUserCount ?: 0}"/>
    </div>
</div>
</body>
</html>