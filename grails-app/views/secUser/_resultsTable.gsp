<div class="table-responsive">
    <table id="resultsTable" class="table">
        <thead>
        <tr>
            <th data-hql-path="secUser.username" onclick="onClickTableHeader(this)"><g:message
                    code="secuser.form.user"/></th>
            <th data-hql-path="secUser.enabled" onclick="onClickTableHeader(this)"><g:message
                    code="secuser.form.enable"/></th>
            <th data-hql-path="secUser.accountExpired" onclick="onClickTableHeader(this)"><g:message
                    code="secuser.form.accountExpired"/></th>
            <th data-hql-path="secUser.accountLocked" onclick="onClickTableHeader(this)"><g:message
                    code="secuser.form.accountLocked"/></th>
            <th data-hql-path="secUser.passwordExpired" onclick="onClickTableHeader(this)"><g:message
                    code="secuser.form.passwordExpired"/></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${secUserList}" status="i" var="secUserInstance">
            <tr>
                <td><g:link action="show"
                            id="${secUserInstance.id}">${fieldValue(bean: secUserInstance, field: "username")}</g:link></td>
                <td>${fieldValue(bean: secUserInstance, field: "enabled")}</td>
                <td>${fieldValue(bean: secUserInstance, field: "accountExpired")}</td>
                <td>${fieldValue(bean: secUserInstance, field: "accountLocked")}</td>
                <td>${fieldValue(bean: secUserInstance, field: "accountExpired")}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
<g:hiddenField name="total" value="${secUserInstanceTotal}"/>
<g:hiddenField name="offset" value="${offset}"/>
<g:hiddenField name="orderBy" value="${orderBy}"/>
<g:hiddenField name="orderMode" value="${orderMode}"/>
