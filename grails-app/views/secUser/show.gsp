<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'secUser.label', default: 'SecUser')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-secUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>
<g:render template="/layouts/nav"/>
<div id="show-secUser" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list dossier">
        <g:if test="${secUserInstance?.username}">
            <li class="fieldcontain row">
                <span id="username-label"
                      class="property-label col-md-6"><g:message
                        code="secUserInstance.username.label" default="User"/></span> <span
                    class="property-value col-md-6" aria-labelledby="username-label"><g:fieldValue
                        bean="${secUserInstance}" field="username"/></span></li>
        </g:if>
        <g:if test="${secUserInstance?.enabled}">
            <li class="fieldcontain row"><span id="enabled-label"
                                               class="property-label col-md-6"><g:message
                        code="secUserInstance.enabled.label" default="Enabled"/></span> <span
                    class="property-value col-md-6" aria-labelledby="enabled-label"><g:fieldValue
                        bean="${secUserInstance}" field="enabled"/></span></li>
        </g:if>

        <li class="fieldcontain row"><span id="accountLocked-label"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.accountLocked.label"
                    default="Account locked"/></span> <span class="property-value col-md-6"
                                                            aria-labelledby="accountLocked-label"><g:fieldValue
                    bean="${secUserInstance}" field="accountLocked"/></span></li>


        <li class="fieldcontain row"><span id="passwordExpired-label"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.passwordExpired.label"
                    default="Password expired"/></span> <span class="property-value col-md-6"
                                                              aria-labelledby="passwordExpired-label"><g:fieldValue
                    bean="${secUserInstance}" field="passwordExpired"/></span></li>


        <li class="fieldcontain row"><span id="accountExpired"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.accountExpired.label"
                    default="Acount expired"/></span> <span class="property-value col-md-6"
                                                            aria-labelledby="accountExpired-label"><g:fieldValue
                    bean="${secUserInstance}" field="accountExpired"/></span></li>

        <li class="fieldcontain row"><span id="name"
                                           class="property-label col-md-6"><g:message
                    code="secuser.create.name"
                    default="Name"/></span> <span class="property-value col-md-6"
                                                  aria-labelledby="name-label"><g:fieldValue
                    bean="${secUserInstance}" field="name"/></span></li>

        <li class="fieldcontain row"><span id="lastName"
                                           class="property-label col-md-6"><g:message
                    code="secuser.create.lastName"
                    default="Last Name"/></span> <span class="property-value col-md-6"
                                                       aria-labelledby="lastName-label"><g:fieldValue
                    bean="${secUserInstance}" field="lastName"/></span></li>

        <li class="fieldcontain row"><span id="email"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.email.label"
                    default="Email"/></span> <span class="property-value col-md-6"
                                                   aria-labelledby="email-label"><g:fieldValue
                    bean="${secUserInstance}" field="email"/></span></li>

        <li class="fieldcontain row"><span id="phone"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.email.label"
                    default="Phone"/></span> <span class="property-value col-md-6"
                                                   aria-labelledby="phone-label"><g:fieldValue
                    bean="${secUserInstance}" field="phone"/></span></li>

        <li class="fieldcontain row"><span id="authority"
                                           class="property-label col-md-6"><g:message
                    code="secUserInstance.accountExpired.label"
                    default="Authority"/></span> <span class="property-value col-md-6"
                                                       aria-labelledby="authority-label"><g:fieldValue
                    bean="${role}" field="authority"/></span></li>
    </ol>
    <g:form useToken="${true}">
        <fieldset class="buttons text-center">
            <g:hiddenField name="id" value="${secUserInstance?.id}"/>
            <g:link class="btn btn-edit" action="edit" id="${secUserInstance?.id}">
                <i class="fas fa-edit"></i>    <g:message code="default.button.edit.label" default="Edit"/>
            </g:link>
            <g:actionSubmit class="btn btn-delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
