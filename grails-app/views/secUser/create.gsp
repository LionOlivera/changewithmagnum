<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'secUser.label', default: 'SecUser')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#create-secUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
    <g:render template="/layouts/nav"/>
        <div id="create-secUser" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.secUser}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.secUser}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form useToken="${true}" action="save">
                <fieldset class="form">

                    <div class="row">
                        <div class="col-md-6">
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'username', 'error')} required">
                                <label class="col-md-5" for=username> <g:message
                                        code="secuser.create.user" default="User" /> <span
                                        class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="username" required=""
                                                 value="${secUserInstance?.username}" class="form-control" />
                                </div>
                            </div>
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'password', 'error')} required">
                                <label class="col-md-5 " for=password> <g:message
                                        code="secuser.create.password" default="Password" /> <span
                                        class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="password" required=""
                                                 value="${secUserInstance?.password}" class="form-control"/>
                                </div>
                            </div>
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'name', 'error')} required">
                                <label for="name" class="col-md-5 col-form-label"> <g:message
                                        code="secuser.create.name" default="Nom" /> <span
                                        class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="name" required=""
                                                 value="${secUserInstance?.name}" class="form-control"/>
                                </div>
                            </div>
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'lastName', 'error')} required">
                                <label for="lastName" class="col-md-5 col-form-label"> <g:message
                                        code="secuser.create.lastName"  /> <span
                                        class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="lastName" required=""
                                                 value="${secUserInstance?.lastName}" class="form-control" />
                                </div>
                            </div>
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'email', 'error')} required">
                                <label for="email" class="col-md-5 col-form-label"> <g:message
                                        code="secuser.create.email" default="Email" /> <span
                                        class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="email" required=""
                                                 value="${secUserInstance?.email}" class="form-control" />
                                </div>
                            </div>
                            <div
                                    class="form-group row fieldcontain ${hasErrors(bean: secUserInstance, field: 'phone', 'error')} required">
                                <label for="phone"  class="col-md-5 col-form-label"> <g:message
                                        code="secuser.create.phone" default="Téléphone portable" />
                                    <span class="required-indicator">*</span>
                                </label>
                                <div class="col-md-7">
                                    <g:textField name="phone" required=""
                                                 value="${secUserInstance?.phone}" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div
                                            class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'enabled', 'error')} required">
                                        <label class="check-box-label container" for="enabled"> <g:message
                                                code="secuser.create.enable"
                                                default="Enabled" /> <span
                                                class="required-indicator"></span>
                                            <g:checkBox name="enabled"
                                                        value="${secUserInstance?.enabled}" />
                                            <span class="checkmark"></span>

                                        </label>

                                    </div>
                                    <div
                                            class="form-group fieldcontain  ${hasErrors(bean: secUserInstance, field: 'accountExpired', 'error')} required">

                                        <label class="check-box-label container" for="accountExpired"> <g:message
                                                code="secuser.create.accountExpired"
                                                default="Account Expired" /> <span
                                                class="required-indicator"></span>
                                            <g:checkBox name="accountExpired"
                                                        value="${secUserInstance?.accountExpired}" />
                                            <span class="checkmark"></span>
                                        </label>

                                    </div>
                                    <div
                                            class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'accountLocked', 'error')} required">

                                        <label class="check-box-label container" for="accountLocked"> <g:message
                                                code="secuser.create.accountLocked"
                                                default="Locked" /> <span
                                                class="required-indicator"></span>

                                            <g:checkBox name="accountLocked"
                                                        value="${secUserInstance?.accountLocked}" />

                                            <span class="checkmark"></span>
                                        </label>

                                    </div>

                                    <div
                                            class="form-group fieldcontain ${hasErrors(bean: secUserInstance, field: 'passwordExpired', 'error')} required">

                                        <label class="check-box-label container" for="passwordExpired"> <g:message
                                                code="secuser.create.passwordExpired"
                                                default="Password Expired" /> <span
                                                class="required-indicator"></span>
                                            <g:checkBox name="passwordExpired"
                                                        value="${secUserInstance?.passwordExpired}" />
                                            <span class="checkmark"></span>
                                        </label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <g:each in="${rolesInstance}" var="role" status="i">
                                        <div class="fieldcontain form-group">
                                            <label class="check-box-label container">

                                                <g:message code="dossier.customer.phone.label2" default="${role.authority}" />
                                                <g:radio name="roles" value="${role.id}" checked="false" />
                                                <span class="checkmark radio"></span>
                                            </label>
                                        </div>
                                    </g:each>
                                </div>

                            </div>


                        </div>
                    </div>




                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="btn btn-save"
                                    value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>








        </div>
    </body>
</html>
