<g:form useToken="${true}" action="save">
    <fieldset class="form">
    <%@ page import="com.changeIT.Orders" %>
    <div id="flotante">

        <div class=" ${hasErrors(bean: orders, field: 'cantidad', 'error')} required">
            <table class="table">
                <thead>
                <tr>
                    <th>Producto<i></i></th>
                    %{--<th>Tamanio<i></i></th>--}%
                    <th>Cantidad Seleccionada</th>
                    <th>Precio</th>
                </tr>
                </thead>
                <tbody>

                <g:each in="${orderInstanceList}" status="i" var="stockInstance">
                    <tr>
                        <td><g:link controller="stock" action="show"
                                    id="${stockInstance.product.id}">${fieldValue(bean: stockInstance?.product, field: "articulo")}</g:link></td>
                        <td>${fieldValue(bean: stockInstance, field: "cantidad")}</td>
                        <td>${fieldValue(bean: stockInstance?.product, field: "precio")}</td>
                        <g:hiddenField name="product_${i}" value="${stockInstance?.product.id}"/>
                        <g:hiddenField name="cantidad_${i}" value="${stockInstance.cantidad}"/>
                    </tr>

                </g:each>

                </tbody>
            </table>

        </div>

    </div>
    </div>

</fieldset>
    <g:if test="${orderInstanceList != null}">
        <g:if test="${orderInstanceList.size() > 0}">
            <g:hiddenField name="cantidad_orden" value="${orderInstanceList.size()}"/>
            <fieldset class="buttons">
                <g:submitButton name="create" class="save"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            </fieldset>
        </g:if>
    </g:if>
</g:form>