<g:form useToken="${true}" action="save">
    <fieldset class="form">
    <%@ page import="com.changeIT.Orders" %>
    <div id="flotante">

        <div class=" ${hasErrors(bean: orders, field: 'cantidad', 'error')} required">
            <table class="table">
                <col style="width:10%">
                <col style="width:40%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <col style="width:10%">
                <thead>
                <tr>
                    <th>Seleccionar<i></i></th>
                    <th data-hql-path="stock.articulo" onclick="onClickTableHeader(this)">Producto<i></i></th>
                    <th data-hql-path="orders.cantidad" onclick="onClickTableHeader(this)">Cantidad<i></i></th>
                    <th data-hql-path="stock.precio" onclick="onClickTableHeader(this)">Precio<i></i></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${stockInstanceList}" status="i" var="stockInstance">
                    <tr>
                        <td><g:checkBox id="productoPedido_${stockInstance.id}" name="productoPedido" value="${stockInstance.id}"   onclick="filter(this)" checked="false"/></td>
                        <td><g:link controller="stock" action="show"  id="${stockInstance.id}">${fieldValue(bean: stockInstance?.product, field: "articulo")}</g:link></td>
                        <td>  <g:select name="stock.cantidad" from="${1..stockInstance.cantidad}"  id="cant_${stockInstance.id}" noSelection="['':'-Selecciona tu cantidad-']"/></td>
                       <td>${fieldValue(bean: stockInstance?.product, field: "precio")}</td>
                    </tr>
                </g:each>

                </tbody>
            </table>

        </div>

    </div>
    </div>

</fieldset>
    <g:hiddenField name="total" value="${stockInstanceTotal}"/>
    <g:hiddenField name="offset" value="${offset}"/>
    <g:hiddenField name="orderBy" value="${orderBy}"/>
    <g:hiddenField name="orderMode" value="${orderMode}"/>
</g:form>