<%@ page import="com.changeIT.Customer" %>

<div class="row">
    <div class="col-md-8 ">

        <div class="fieldcontain row ${hasErrors(bean: orders, field: 'codigoPedido', 'error')} required">
            <label for="codigoPedido" class="col-md-4 control-label"><g:message
                    code="order.form.codigo" default="Codigo Pedido"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="codigoPedido" class="form-control" required="" disabled="true"
                             value="${orders?.codigoPedido}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: orders, field: 'created', 'error')} required">
            <label for="codigoPedido" class="col-md-4 control-label"><g:message
                    code="order.form.creacion" default="Fecha de Creacion"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="created" class="form-control" required="" disabled="true"
                             value="${orders?.created}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: orders, field: 'montoTotal', 'error')} required">
            <label for="montoTotal" class="col-md-4 control-label"><g:message
                    code="order.form.montoPedido" default="Monto Pedido"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="montoTotal" class="form-control" required="" disabled="true"
                             value="${orders?.montoTotal}"/>
            </div>

        </div>

        <div class="fieldcontain row ${hasErrors(bean: orders?.state, field: 'description', 'error')} required">
            <label for="montoTotal" class="col-md-4 control-label"><g:message
                    code="order.form.estado" default="Estado"/> <span
                    class="required-indicator">*</span>
            </label>

            <div class="col-md-8">
                <g:textField name="description" class="form-control" required="" disabled="true"
                             value="${orders?.state?.description}"/>
            </div>

        </div>

        <hr>
        <g:each in="${orders?.resumenPedidos}" var="resumen" status="i">
            <div class="form-group fieldcontain">
                <label for="articulo" class="col-md-4 control-label"><g:message code="order.form.articulo.1"
                                                                                default="Articulo"/> <span
                        class="required-indicator">*</span>
                </label>

                <div class="col-md-8">
                    <g:textField name="articulo" class="form-control" required="" disabled="true"
                                 value="${resumen?.productoOrdenado?.articulo}"/>
                </div>

                <label for="cantidad" class="col-md-4 control-label"><g:message code="order.form.articulo.2"
                                                                                default="cantidad"/> <span
                        class="required-indicator">*</span>
                </label>

                <div class="col-md-8">
                    <g:textField name="cantidad" class="form-control" required="" disabled="true"
                                 value="${resumen?.cantidad}"/>
                </div>
            </div>
        </g:each>

    </div>

    <div class="col-md-4">
        <div class="panel">
            <h2><g:message code="order.form.estado"/></h2>
            <g:each in="${stateIntance}" var="state" status="i">
                <div class="form-group fieldcontain">
                    <label for="articulo">${state.description?.toString()} <span
                            class="required-indicator">*</span>
                        <g:if test="${orders?.state?.id == state.id}">
                            <input type="radio"  id="${state.id}" name="estadoID" value="${state.id}"
                                   checked/>
                            <span class="checkmark"></span>
                        </g:if>
                        <g:if test="${orders?.state?.id != state.id}">
                            <input type="radio" id="${state.id}" name="estadoID" value="${state.id}"
                                        />
                            <span class="checkmark"></span>
                        </g:if>
                    </label>
                </div>
            </g:each>
        </div>
    </div>
</div>



</div>