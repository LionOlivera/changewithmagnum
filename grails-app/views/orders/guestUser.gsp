<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>

    <asset:stylesheet src="style.css"/>
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
          crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>

    <title>Change it - Pedidos</title>
</head>

<body>
<a href="#create-secUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>
<div class="container-fluid">
<div id="create-secUser" class="content scaffold-create" role="main">
    <div class="row justify-content-center">
        <div class="col-md-6 ">
            <div class="formulario">
                <h1>Completa con tus datos</h1>
                <br><br>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.secUser}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.secUser}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                    error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
                <g:form useToken="${true}" action="saveGuestOrder">
                    <fieldset class="form">




                         <label for="email" class="col-md-12 col-form-label"><g:message
                        code="secuser.create.email" default="Email"/> <span
                        class="required-indicator">*</span>
                    </label>

                    <div class="col-md-12">
                        <g:textField name="email" required=""
                                     value="${secUserInstance?.email}" class="form-control"/>
                    </div>



                    <label for="phone" class="col-md-12 col-form-label"><g:message
                            code="secuser.create.phone" default="Téléphone portable"/>
                        <span class="required-indicator">*</span>
                    </label>

                    <div class="col-md-12">
                        <g:textField name="phone" required=""
                                     value="${secUserInstance?.phone}" class="form-control"/>
                    </div>

                    <label for="phone" class="col-md-12 col-form-label"><g:message
                            code="secuser.create.adress" default="Direccion"/>
                        <span class="required-indicator">*</span>
                    </label>

                    <div class="col-md-12">
                        <g:textField name="address" required=""     class="form-control"/>
                    </div>


                        <div class="col-md-12" style="margin-top: 20px">
                            <g:submitButton name="create" class="btn btn-primary"
                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                        </div>

                    </fieldset>


                </g:form>
            </div>
        </div>
    </div>


</div>
</div>
</body>
</html>
