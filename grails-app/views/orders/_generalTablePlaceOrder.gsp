<div class="product_list">
    <%@ page import="com.changeIT.Stock" %>

    <div class="container-fluid">
        <div class="row">

            %{--<g:if test="${orderInstanceList.size() > 0 || orderInstanceList != null}">--}%
            <div class="table-responsive">
                <table class="col-md-12 table">
                    <thead>
                    <tr>
                        <th>Productos pedidos</th>
                        <th>Precio por unidad</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${orderInstanceList}" status="i" var="stockInstance">
                        <g:if test="${stockInstance?.cantidad > 0}">
                            <tr>
                                <td>
                                    <div class="carrito_imagen">
                                        <img src="<g:createLink controller="product" action="featuredImage"
                                                                id="${stockInstance?.product.id}"/>" alt="" class="img-responsive">
                                    </div>

                                    <div class="carrito_descripcion">
                                        <p>${stockInstance?.product.articulo}</p>

                                    </div>
                                </td>
                                <td>$ ${stockInstance?.product.precio}</td>
                                <td>${stockInstance?.cantidad} unidades</td>
                                <td><span>$${String.format("%.2f",(stockInstance?.product.precio * stockInstance?.cantidad))}</span> </td>
                                <td class="eliminar_item"><a href="<g:createLink controller="orders" action="cleanPRD" id="${stockInstance?.product.id}"/>" ><i class="fas fa-minus-circle"></i> Eliminar</a></td>
                            </tr>
                        </g:if>
                    </g:each>
                    </tbody>

                </table>
            </div>

            %{--</g:if>--}%
        </div>
    </div>
</div>





