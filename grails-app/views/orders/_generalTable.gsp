<div class="product_list">
    <%@ page import="com.changeIT.Stock" %>

        <div class="row">

            <g:if test="${stockInstanceList.size() > 0 || stockInstanceList != null}">
                <g:each in="${stockInstanceList}" status="i" var="stockInstance">
                    <g:if test="${stockInstance?.cantidad > 0}">
                        <article class="col-xl-3 col-lg-3 col-md-4">
                            <div class="product_item">
                            <div class="product_image">
                                <g:if test="${stockInstance?.product.featuredImageBytes}">
                                    <img src="<g:createLink controller="product" action="featuredImage"

                                                            id="${stockInstance?.product.id}"/>" class="img-responsive"/>
                                </g:if>
                            </div>

                            <div class="product_text">
                                <h2><g:link controller="product" action="show"
                                            id="${stockInstance?.product.id}">${fieldValue(bean: stockInstance?.product, field: "articulo")}</g:link></h2>

                                <div class="product_detail">
                                    <p>${stockInstance?.product.obs}</p>

                                    <p class="precio">$ ${stockInstance?.product.precio}</p>
                                </div>
                            </div>

                            <div class="product_action">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 mb-2">
                                        <g:select name="stock.cantidad_${stockInstance.id}" from="${1..stockInstance.cantidad}"
                                                  id="cant_${stockInstance.id}" noSelection="['': '-Selecciona tu cantidad-']"
                                                  optionKey="" class="form-control"/>


                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        %{--<g:link controller="stock"  action="filterListRemoteGeneral" class="btn add_button " id="${stockInstance?.id}">Añadir</g:link>--}%
                                        <g:submitButton class="btn add_button btn-block" id="${stockInstance.id}" name="productoPedido"
                                                        value="Añadir" onclick="filterX(this)"/>
                                    </div>
                                </div>

                            </div>
                            </div>
                        </article>
                    </g:if>
                </g:each>
            </g:if>
        </div>
</div>


<g:hiddenField name="total" value="${stockInstanceTotal}"/>
<g:hiddenField name="offset" value="${offset}"/>
<g:hiddenField name="orderBy" value="${orderBy}"/>
<g:hiddenField name="orderMode" value="${orderMode}"/>


