<!doctype html>
<html lang="es">
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:stylesheet src="style.css"/>
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
          crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>

    <title>Change it - Pedidos</title>
</head>

<body>
<g:render template="/layouts/nav"/>

<div class="container-fluid">

    <div class="content">

    <div class="row">
        <div class="col-md-12">
            <h1>Mi pedido</h1>
        </div>
    </div>

<sec:ifLoggedIn>
    <g:form useToken="${true}" action="save">
        <div class="carrito">


                <div class="row top_carrito">

                    <div class="col-md-12">
                        <ul class="total_pedido">
                            <li class="total_numero">Total: <strong>${total}</strong></li>

                            <li><g:submitButton name="create" class="btn btn-primary"
                                                value="${message(code: 'default.button.create.label', default: 'Create')}"/></li>

                        </ul>

                    </div>
                </div>

                <g:render template="generalTablePlaceOrder"/>
            </div>
    </g:form>
</sec:ifLoggedIn>
<sec:ifNotLoggedIn>
    <g:form useToken="${true}" action="guestUser">
        <div class="carrito">

                <div class="row top_carrito">

                    <div class="col-md-12">
                        <ul class="total_pedido">
                            <li class="total_numero">Total: <strong>${total}</strong></li>

                            <li><g:submitButton name="create" class="btn btn-primary"
                                                value="${message(code: 'default.button.create.label', default: 'Create')}"/></li>

                        </ul>

                    </div>
                </div>

                <g:render template="generalTablePlaceOrder"/>
            </div>
    </g:form>
</sec:ifNotLoggedIn>
    <g:form useToken="${true}" action="cleanOrderPlace">
        <div class="limpiar_wrapper">
            <g:submitButton name="delete" action="cleanOrderPlace" class=" limpiar"
                            value="${message(code: 'checkout.clean.cart', default: 'Limpiar')}"/>
        </div>
    </g:form>
</div>
</div>

</body>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</html>
