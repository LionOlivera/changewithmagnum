<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'orders.label', default: 'Orders')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-orders" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                             default="Skip to content&hellip;"/></a>

<g:render template="/layouts/nav"/>

<div id="list-orders" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
        <g:link class="create btn" action="create"><i class="fas fa-plus-circle"></i><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link>
    </sec:ifAnyGranted>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Codigo Pedido</th>
                    <th>Fecha</th>
                    <th>Customer</th>
                    <th></th>
                    <th>Monto Total</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${ordersList}" status="i" var="orderInstance">
                    <tr>
                        <td>
                            ${fieldValue(bean: orderInstance, field: "codigoPedido")}
                        </td>
                        <td>
                            ${fieldValue(bean: orderInstance, field: "created")}
                        </td>
                        <td>
                            <g:link action="show" controller="customer"  id="${orderInstance?.customer?.id}">${fieldValue(bean: orderInstance?.customer, field: "name")}</g:link>
                        </td>
                        <td>
                            <g:link action="index" controller="resumenPedido"   id="${orderInstance?.codigoPedido}">Detalle</g:link>
                        </td>
                    </tr>
                </g:each>

                </tbody>
            </table>

        </div>
    </div>


    <div class="pagination">
        <g:paginate total="${ordersCount ?: 0}"/>
    </div>
</div>
</body>
</html>