<div class="card">
    <div class="table-responsive">
        <%@ page import="com.changeIT.Orders" %>

        <table class="table">
            <thead>
            <tr>
                <th>Codigo Pedido</th>
                <th>Fecha</th>
                <th>Customer</th>
                <th></th>
                <th>Monto Total</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>

            <g:each in="${ordersList}" status="i" var="orderInstance">
                <tr>
                    <td>
                        <g:link controller="orders" action="show" id="${orderInstance?.id}">${fieldValue(bean: orderInstance, field: "codigoPedido")}</g:link>
                    </td>
                    <td>
                        <g:formatDate format="dd-MM-yyyy" date="${orderInstance.created}"/>
                    </td>
                    <td>
                        <g:link action="show" controller="customer"
                                id="${orderInstance?.customer?.id}">${fieldValue(bean: orderInstance?.customer, field: "name")}</g:link>
                    </td>
                    <td>
                        <g:link action="index" controller="resumenPedido"  id="${orderInstance?.codigoPedido}">Detalle</g:link>
                    </td>
                    <td>
                        ${fieldValue(bean: orderInstance, field: "montoTotal")}
                    </td>
                    <td>
                        ${fieldValue(bean: orderInstance?.state, field: "description")}

                    </td>
                </tr>
            </g:each>

            </tbody>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>Total :</strong></td>
            <td>
                <strong><g:formatNumber number="${total}" type="number" minIntegerDigits="2"/></strong>
            </td>
        </table>

    </div>

</div>

<g:hiddenField name="total" value="${ordersCount}"/>
<g:hiddenField name="offset" value="${offset}"/>
<g:hiddenField name="orderBy" value="${orderBy}"/>
<g:hiddenField name="orderMode" value="${orderMode}"/>
