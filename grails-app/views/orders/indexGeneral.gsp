<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:stylesheet src="style.css"/>
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
          crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
    <g:hiddenField name="offset" value="${offset}"/>
    <script>
        var codigoPedido = "${codPedido}";
        var currentSortingHqlPath;

        var data = {
            stockId: "",
            flag: ""
        };
        var list = [data];

        /* $(document).ready(function () {
             $('#productoPedido').change(function () {
                 currentSortingHqlPath = $('#productoPedido').val();
                 filterRemotely(currentSortingHqlPath);
             });
         });*/

        function filterX(val) {

            filterRemotelyStockUnit(val.id, val);

        }

        function filterTipo(val) {

            filterRemotelyTipo(val.id);

        }

        function filterRemotelyTipo(tipo) {
            var data = {
                id: tipo,

            };

            $('#resultsTableDivbyTipo').load($('#remoteFilterUrlbyTipo').val(), data, function () {

            });

        }

        function filterRemotelyStockUnit(stockID, val) {
            var fieldValue = $('#cant_' + stockID).val();
            if (fieldValue == '') {
                window.alert("completar cantidad");
                val.checked = false
            } else {
                var data = {
                    stockId: stockID,
                    cantidadSeleccionada: fieldValue,
                    codigoPedido: codigoPedido
                };
                %{--<g:remoteFunction controller="orders" action="filterListRemote" params="data"/>--}%
                $('#resultsTableDiv2').load($('#remoteFilterUrlGeneral').val(), data, function () {

                });
            }
        }


    </script>

    <script>
        function createAppliedFilter() {
            var selectedField = $('#filterField option:selected');
            var fieldValue = $('#filterValue').val();

            var currentFilters = extractFilters();
            if (!currentFilters.hasOwnProperty(selectedField.val())) {
                var appliedFilterItem = drawAppliedFilter(selectedField.text(), fieldValue);
                appliedFilterItem.data(selectedField.val(), fieldValue);
            }
        }

        function drawAppliedFilter(fieldTitle, fieldValue) {
            var appliedFiltersList = $('.filtros-aplicados ul');

            var newAppliedFilterItem = $('<li>' + fieldTitle + ' ' + fieldValue + '<i class="fas fa-times"></i></li>');
            newAppliedFilterItem.appendTo(appliedFiltersList);

            return newAppliedFilterItem;
        }

        function computePages(total, offset, max) {
            var completePages = Math.floor(total / max);
            var extraPage = (total % max) ? 1 : 0;
            var totalPages = completePages + extraPage;
            var currentPage = Math.floor(offset / max + 1);

            return {totalPages: totalPages, currentPage: currentPage}
        }

        function updatePages() {
            var pagesData = computePages(Number($('#total').val()), Number($('#offset').val()), Number($('#numberPerPage').val()));

            $('#currentPage').html(pagesData.currentPage);
            $('#totalPages').html(pagesData.totalPages)
        }

        function updateTotalResults() {
            var total = $('#total').val();

            $('#dossierInstanceTotal').html(total);
        }

        function filterRemotely(offset, max, orderBy, orderMode) {
            var filters = extractFilters();

            var data = {
                offset: offset,
                max: max,
                orderBy: orderBy,
                orderMode: orderMode,
                filters: JSON.stringify(filters)
            };

            $('#resultsTableDiv').load($('#remoteFilterUrl').val(), data, function () {
                updateTotalResults();
                updatePages();
            });
        }

        function extractFilters() {
            var filters = {};
            $('.filtros-aplicados li').each(function () {
                $.extend(filters, $(this).data());
            });

            return filters;
        }

        function addPage(addend) {
            var total = Number($('#total').val());
            var max = Number($('#numberPerPage').val());
            var newOffset = Number($('#offset').val()) + addend * max;

            if (newOffset >= 0 && newOffset < total) {
                filterRemotely(newOffset, max, $('#orderBy').val(), $('#orderMode').val());
            }
        }

        function onClickTableHeader(headerHtmlElement) {
            changeSorting($(headerHtmlElement).data('hqlPath'));
            filterRemotely(0, $('#numberPerPage').val(), $('#orderBy').val(), $('#orderMode').val());
        }

        function changeSorting(clickedHqlPath) {
            var currentSortingHqlPath = $('#orderBy').val();
            var currentSortingMode = $('#orderMode').val();

            if (currentSortingHqlPath === clickedHqlPath) {
                $('#orderMode').val(currentSortingMode === 'asc' ? 'desc' : 'asc');
            } else {
                $('#orderMode').val('asc');
            }

            $('#orderBy').val(clickedHqlPath);
        }

        $(document).ready(function () {
            $('.filtros-btn').click(function () {
                $(".filtros").addClass('open');
            });
            $('.filtros').find('.cerrar-filtros').click(function () {
                $('.filtros').removeClass('open');
            });

            $('.filtros-aplicados').on('click', '.fa-times', function () {
                $(this).parent('li').remove();
                filterRemotely(0, $('#numberPerPage').val(), $('#orderBy').val(), $('#orderMode').val());
            });

            $('.agregar-filtro').click(function () {
                createAppliedFilter();
                filterRemotely(0, $('#numberPerPage').val(), $('#orderBy').val(), $('#orderMode').val())
            });

            $('#numberPerPage').change(function () {
                var newNumber = Number(this.value);

                if (!Number.isNaN(newNumber)) {
                    filterRemotely(0, $('#numberPerPage').val(), $('#orderBy').val(), $('#orderMode').val())
                }
            });

            $('#prevPage').click(function () {
                addPage(-1);
            });
            $('#nextPage').click(function () {
                addPage(1);
            });

            updateTotalResults();
            updatePages();
        });
    </script>
</head>

<body>
<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>


<div id="resultsTableDiv2">
    <g:render template="/layouts/nav"/>
</div>

<div class="categorias_menu">
    <ul>
        <li><a  id="food" onclick="filterTipo(this)">Alimentos</a></li>
        <li><a  id="nonfood" onclick="filterTipo(this)">Non-Food</a></li>
        <li><a  id="bebidas" onclick="filterTipo(this)">Bebidas</a></li>
        <li><a  id="limpieza" onclick="filterTipo(this)">Limpieza</a></li>
        <li><a  id="accesorios" onclick="filterTipo(this)">Accesorios</a></li>
    </ul>
</div>

<div class="container-fluid ">

    <div id="list-product" class="content scaffold-list" role="main">

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_VENDEDOR">
            <g:link class="create btn" action="create"><i class="fas fa-plus-circle"></i> <g:message
                    code="default.new.label"
                    args="[entityName]"/></g:link>
        </sec:ifAnyGranted>
        <div class="row">
            <div class="col-xl-3 col-lg-12 col-md-12">
                <h1 class="main_title"><g:message code="default.list.label" args="[entityName]"/></h1>
            </div>

            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="row opciones">

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <button class="filtros-btn btn btn-default">
                            <i class="fas fa-sliders-h"></i> <g:message code="filter.btn"/>
                        </button>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 total">
                        <g:message code="filter.quantity.dossier"/><span
                            id="dossierInstanceTotal"><strong>${productInstanceTotal}</strong></span>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 numero-paginas">
                        <g:message code="filter.quantity.page"/> <input id="numberPerPage"
                                                                        class="ng-valid ng-not-empty ng-dirty ng-valid-number ng-touched"
                                                                        ng-model="block.limit" ng-attr-type="number"
                                                                        ng-change="block.update()" type="number"
                                                                        value="${max}"
                                                                        min="1">
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 prevnext">
                        <g:message code="filter.page"/> <span id="currentPage"></span> / <span id="totalPages"></span>

                        <div class="btn-group">
                            <button id="prevPage" class="btn btn-default">
                                <i class="fas fa-angle-left"></i>
                            </button>
                            <button id="nextPage" class="btn btn-default">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row filtros-aplicados">
            <ul>
            </ul>
        </div>

        <div class="card filtros" id="filtros">
            <div class="row">
                <div class="col-md-5">
                    <label for="filterpar"><g:message code="filter.label"/>
                        <div id="filterField" class="select">
                            <select name="" id="">
                                <option value="product.codigo"><g:message code="customer.list.table.1"/></option>
                                <option value="product.articulo"><g:message code="customer.list.table.2"/></option>
                                <option value="product.precio"><g:message code="customer.list.table.3"/></option>
                                <option value="product.mdl"><g:message code="customer.list.table.5"/></option>
                                <option value="product.tipo"><g:message code="customer.list.table.6"/></option>
                                <option value="product.obs"><g:message code="customer.list.table.7"/></option>
                            </select>
                        </div>
                    </label>
                </div>

                <div class="col-md-5">
                    <label for="valeur"><g:message code="customer.list.table.filter"/> <input id="filterValue"
                                                                                              type="text"
                                                                                              name="valeur"
                                                                                              placeholder="">
                    </label>
                </div>
                <button class="btn btn-default agregar-filtro">
                    <i class="fas fa-check"></i>
                </button>
                <button class="btn btn-default cerrar-filtros">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>

        <div id="resultsTableDiv" class="">
            <div id="resultsTableDivbyTipo" class="">
            <g:render template="generalTable"/>
            </div>
        </div>
        <g:hiddenField name="remoteFilterUrl"        value="${createLink(controller: 'orders', action: 'filterListRemoteIndexGeneralPRD')}"/>
        <g:hiddenField name="remoteFilterUrlGeneral" value="${createLink(controller: 'orders', action: 'filterListRemoteIndexGeneral')}"/>
        <g:hiddenField name="remoteFilterUrlbyTipo"  value="${createLink(controller: 'orders', action: 'getProductbyTipo')}"/>
        <g:hiddenField name="total" value="${productInstanceTotal}"/>
        <g:hiddenField name="offset" value="${offset}"/>
        <g:hiddenField name="orderBy" value="${orderBy}"/>
        <g:hiddenField name="orderMode" value="${orderMode}"/>
    </div>

</div>

</body>
</html>