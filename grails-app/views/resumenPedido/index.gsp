<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'resumenPedido.label', default: 'ResumenPedido')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-resumenPedido" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                    default="Skip to content&hellip;"/></a>

<g:render template="/layouts/nav"/>

<div id="list-resumenPedido" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>

                </thead>
                <tbody>
                <g:each in="${resumenPedidoList}" status="i" var="resumeInstance">
                    <tr>
                        <td>
                            <g:link action="show" controller="product"
                                    id="${resumeInstance?.productoOrdenado.id}">${fieldValue(bean: resumeInstance?.productoOrdenado, field: "articulo")}</g:link>
                        </td>
                        <td>
                            ${fieldValue(bean: resumeInstance, field: "cantidad")}
                        </td>

                    </tr>
                </g:each>

                </tbody>
            </table>

        </div>
    </div>



</div>
</body>
</html>