package com.changeIT

class BootStrap {

    def init = { servletContext ->


        SecUser admin = SecUser.findByUsername('admin')? SecUser.findByUsername('admin') : new SecUser(username: 'admin', password: 'Secret0!',email: 'olivera.lionel@gmail.com',attempts: 0, enabled: true).save(flush:true)
        SecUser user = SecUser.findByUsername('user')? SecUser.findByUsername('user') : new SecUser(username: 'user', password: 'Secret0!',email: 'olivera.lionel+1@gmail.com',attempts: 0, enabled: true).save(flush:true)
        SecUser vendedor = SecUser.findByUsername('vendedor')? SecUser.findByUsername('vendedor') :  new SecUser(username: 'vendedor', password: 'Secret0!',email: 'olivera.lionel+2@gmail.com',attempts: 0, enabled: true).save(flush:true)
        SecUser api = SecUser.findByUsername('api')? SecUser.findByUsername('api') :  new SecUser(username: 'api', password: 'Secret0!',email: 'olivera.lionel+3@gmail.com',attempts: 0, enabled: true).save(flush:true)

        SecRole adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?  SecRole.findByAuthority('ROLE_ADMIN'): new SecRole(authority: 'ROLE_ADMIN').save(flush:true)
        SecRole userRole = SecRole.findByAuthority('ROLE_CLIENTE') ?  SecRole.findByAuthority('ROLE_CLIENTE'): new SecRole(authority: 'ROLE_CLIENTE').save(flush:true)
        SecRole vendedorrRole = SecRole.findByAuthority('ROLE_VENDEDOR') ?  SecRole.findByAuthority('ROLE_VENDEDOR'): new SecRole(authority: 'ROLE_VENDEDOR').save(flush:true)
        SecRole apiRole = SecRole.findByAuthority('ROLE_API') ?  SecRole.findByAuthority('ROLE_API'): new SecRole(authority: 'ROLE_API').save(flush:true)


        SecUserSecRole secRoleAdmin = SecUserSecRole.findBySecUser(admin)?SecUserSecRole.findBySecUser(admin):SecUserSecRole.create(admin,adminRole,true)
        SecUserSecRole secRolevendedor = SecUserSecRole.findBySecUser(vendedor)?SecUserSecRole.findBySecUser(vendedor):SecUserSecRole.create(vendedor,vendedorrRole,true)
        SecUserSecRole secRoleuser = SecUserSecRole.findBySecUser(user)?SecUserSecRole.findBySecUser(vendedor):SecUserSecRole.create(user,userRole,true)
        SecUserSecRole secRoleapi = SecUserSecRole.findBySecUser(api)?SecUserSecRole.findBySecUser(api):SecUserSecRole.create(api,apiRole,true)



		Configuration config = Configuration.findByProperty("mail.remitente")? Configuration.findByProperty("mail.remitente") :  Configuration.create("mail.remitente", "olivera.lionel@gmail.com" ,true)
        Configuration config2 = Configuration.findByProperty("mail.usuarioConexion")? Configuration.findByProperty("mail.usuarioConexion") : Configuration.create("mail.usuarioConexion","olivera.lionel@gmail.com" ,true)
        Configuration config3 = Configuration.findByProperty("mail.passConexion")? Configuration.findByProperty("mail.passConexion") : Configuration.create("mail.passConexion","SacaEsta2017" ,true)
        Configuration config4 = Configuration.findByProperty("mail.smtp.host")? Configuration.findByProperty("mail.smtp.host") : Configuration.create("mail.smtp.host","smtp.gmail.com",true)
        Configuration config5 = Configuration.findByProperty("mail.smtp.port")? Configuration.findByProperty("mail.smtp.port") : Configuration.create("mail.smtp.port","465",true)
        Configuration config6 = Configuration.findByProperty("mail.smtp.auth")? Configuration.findByProperty("mail.smtp.auth") : Configuration.create("mail.smtp.auth","true",true)
        Configuration config7 = Configuration.findByProperty("mail.debug")? Configuration.findByProperty("mail.debug") : Configuration.create("mail.debug","false",true)
        Configuration config8 = Configuration.findByProperty("mail.smtp.ssl.trust")? Configuration.findByProperty("mail.smtp.ssl.trust") : Configuration.create("mail.smtp.ssl.trust","smtp.gmail.com",true)
        Configuration config9 = Configuration.findByProperty("mail.smtp.socketFactory.port")? Configuration.findByProperty("mail.smtp.socketFactory.port") : Configuration.create("mail.smtp.socketFactory.port","465",true)
        Configuration config10 = Configuration.findByProperty("mail.smtp.socketFactory.fallback")? Configuration.findByProperty("mail.smtp.socketFactory.fallback") : Configuration.create("mail.smtp.socketFactory.fallback","false",true)
        Configuration config11 = Configuration.findByProperty("mail.smtp.socketFactory.class")? Configuration.findByProperty("mail.smtp.socketFactory.class") : Configuration.create("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory",true)
        Configuration config12 = Configuration.findByProperty("mailConfig")? Configuration.findByProperty("mailConfig") : Configuration.create("mailConfig","gmail",true)


        State state1 = State.findByDescription("iniciado")? State.findByDescription("iniciado"):new State(code: "1",description: "iniciado").save(flush:true)
        State state2 = State.findByDescription("En proceso")? State.findByDescription("En proceso"):new State(code: "2",description: "En proceso").save(flush:true)
        State state3 = State.findByDescription("Confirmado")? State.findByDescription("Confirmado"):new State(code: "3",description: "Confirmado").save(flush:true)
        State state4 = State.findByDescription("Terminado")? State.findByDescription("Terminado"):new State(code: "4",description: "Terminado").save(flush:true)



    }
    def destroy = {
    }
}
