package com.changeIT

import grails.gorm.services.Service

@Service(ResumenPedido)
interface ResumenPedidoService {

    ResumenPedido get(Serializable id)

    List<ResumenPedido> list(Map args)

    Long count()

    void delete(Serializable id)

    ResumenPedido save(ResumenPedido resumenPedido)

}