package com.changeIT

import grails.gorm.transactions.Transactional

@Transactional
class OrdersFilterService {


    /** Find all the dossiers which meet a set of filters.
     * @param filters the fields to search by. Ex.: [dossier.firmDate: new Date(), customer.email: 'email@email.com']
     * @param pageParams the pagination params. Ex.: [max: 10, offset: 0]
     * @param sortParams the sorting params. Ex.: [hqlPath: 'dossier.firmDate', order: 'asc']
     * @param agent the agent in charge of the dossiers
     * @param count if true: get the number of total results, if false: get the results
     * @return the list of resulting dossiers
     */
    def findOrder(Map filters, Map pageParams, Map sortParams, SecUser agent, Boolean count = false) {
        StringBuilder hqlQueryBuilder = new StringBuilder('SELECT ')
        hqlQueryBuilder << (count ? 'count(orders) ' : 'orders ')
        hqlQueryBuilder << 'FROM Orders as orders '

        Map namedParams = [:]

        filters.each { String hqlPath, value ->
            String namedParam = hqlPath.replaceAll(/\./, '')

            if (value instanceof String) {
                hqlQueryBuilder << " WHERE lower(${hqlPath}) LIKE lower(:${namedParam}) "
                value = "%${value}%"
            } else {
                hqlQueryBuilder << "${hqlPath} = :${namedParam} "
            }
            namedParams[namedParam] = value
        }
        if (sortParams && !count) {
            hqlQueryBuilder << "ORDER BY ${sortParams.hqlPath} ${sortParams.order}"
        }

        Orders.executeQuery(hqlQueryBuilder.toString(),namedParams,pageParams)
    }


}
