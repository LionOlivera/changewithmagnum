package com.changeIT

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import ua_parser.Parser
import ua_parser.Client
import javax.servlet.http.HttpServletRequest

@Transactional
class LoginAccessLogService {

    SpringSecurityService springSecurityService

    LoginAccessLog saveFromRequest(HttpServletRequest request, SecUser user) {
        String userAgent = request.getHeader('User-Agent')
        String ipAddress = request.getHeader('X-FORWARDED-FOR') ?: request.remoteAddr

        save(ipAddress, userAgent, user)
    }

    LoginAccessLog save(String ipAddress, String userAgent, SecUser user) {
        Map userAgentInfo = parseUserAgent(userAgent)

        LoginAccessLog accessLog = new LoginAccessLog([ipAddress: ipAddress, user: user, createdDate: new Date()] + userAgentInfo)
        accessLog.save()

        accessLog
    }

    private Map parseUserAgent(String userAgent) {
        Parser userAgentParser = new Parser()
        Client client = userAgentParser.parse(userAgent)

        [browser: "${client.userAgent.family} ${client.userAgent.major}.${client.userAgent.minor}", operatingSystem: "${client.os.family} ${client.os.major}.${client.os.minor}"]
    }

}
