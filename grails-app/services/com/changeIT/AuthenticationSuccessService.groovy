package com.changeIT

import grails.gorm.transactions.Transactional
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent

@Transactional
class AuthenticationSuccessService implements ApplicationListener<AuthenticationSuccessEvent> {

    void onApplicationEvent(AuthenticationSuccessEvent event) {

        AuthenticationSuccessEvent loginSuccessEvent = event

        Object obj = loginSuccessEvent.getAuthentication().getPrincipal()

        String user = obj.getAt("username").toString().trim()
        SecUser secUser
        secUser = SecUser.findByEmail(user)
        if (secUser == null) {
            secUser = SecUser.findByUsername(user)
            if (secUser != null) {
                updateAttempts(secUser)
            }
        } else {
            updateAttempts(secUser)
        }

    }


    private void updateAttempts(SecUser secUser) {
        if (secUser.getAttempts() != null)
            if (secUser.getAttempts() > 0) {
                secUser.setAttempts(0)
                secUser.save()
            }
    }
}

