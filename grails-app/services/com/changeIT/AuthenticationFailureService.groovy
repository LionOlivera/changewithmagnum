package com.changeIT

import grails.gorm.transactions.Transactional
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent


@Transactional
class AuthenticationFailureService implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {


    void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        AuthenticationFailureBadCredentialsEvent loginFailureEvent = event
        Object name = loginFailureEvent.getAuthentication().getPrincipal()
        SecUser secUser
        secUser = SecUser.findByEmail(name.toString())

        if (secUser == null) {
            secUser = SecUser.findByUsername(name.toString())
            if (secUser != null) {
                updateAttempts(secUser)
            }
        } else {
            updateAttempts(secUser)
        }

    }


    private void updateAttempts(SecUser secUser) {
        if (secUser.getAttempts() < 5) {
            if (secUser.getAttempts() == null) {
                secUser.setAttempts(1)
            } else {
                secUser.setAttempts(secUser.getAttempts() + 1)
            }
        } else {
            if (secUser.getAccountLocked() == false)
                secUser.setAccountLocked(true)
            else {
                // mensaje
            }
        }
        secUser.save()
    }
}
