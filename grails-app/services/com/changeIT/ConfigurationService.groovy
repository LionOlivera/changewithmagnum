package com.changeIT

import grails.gorm.services.Service

@Service(Configuration)
interface ConfigurationService {

    Configuration get(Serializable id)

    List<Configuration> list(Map args)

    Long count()

    void delete(Serializable id)

    Configuration save(Configuration configuration)

}