package com.changeIT

import grails.gorm.transactions.Transactional
import org.springframework.dao.DataIntegrityViolationException

@Transactional
class SecUserCustomService {

    Customer saveUserCustomer(Customer customer, SecRole role) {
       try {
           customer.save()
        } catch (Exception e) {
            redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method createUser() from RegistrationController", e: e.getMessage(), cont: "registration", act: "createUser"])
        }
        if (customer.hasErrors()) {
            return customer
        }

        SecUserSecRole userRole = new SecUserSecRole(secUser: customer.getSecUser(), secRole: role)
        userRole.save()

        return customer
    }
    Customer saveUserCustomerGuest(Customer customer, SecRole role, SecUser user) {
        try {
            user.save(flush: true)
            customer.setSecUser(user )
            customer.save(flush: true)


        } catch (Exception e) {
            redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method createUser() from RegistrationController", e: e.getMessage(), cont: "orders", act: "saveUserCustomerGuest"])
        }
        if (customer.hasErrors()) {
            return customer
        }

        SecUserSecRole userRole = new SecUserSecRole(secUser: user, secRole: role).save(flush: true)

        return customer
    }
    SecUser save(SecUser user, SecRole role) {

        try {
            user.save(flush:true)
            SecUserSecRole userRole = new SecUserSecRole(secUser: user, secRole: role)
            userRole.save(flush:true)
        } catch (Exception e) {
            log.error("Error al crear usuario: ${e.message}", e)
            redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method createUser() from RegistrationController", e: e.getMessage(), cont: "SecUse", act: "save"])
        }
        if (user.hasErrors()) {
            return user
        }



        return user
    }

    def deleteUser(secUserInstance) {
        try {
            if (!agentHasAnyCustomer(secUserInstance)) {
                SecUserSecRole.removeAll(secUserInstance)
                try {
                    secUserInstance.delete(flush: true)
                    return true
                }
                catch (DataIntegrityViolationException e) {
                    return false
                }
            }
        } catch (Exception e) {
            return false
        }
    }
    def isSelected(role, selectedRoles) {
        for (int i = 0; i < selectedRoles.size(); i++) {
            if (selectedRoles.get(i).id == role.id) {
                return true
            }
        }
        return false
    }
    def agentHasAnyCustomer(secUserInstance) {
        Customer cusotmer = Customer.findBySecUser(secUserInstance)
        if (cusotmer.getOrders().size() > 0) {
            return false
        } else {
            return true
        }
    }
}
