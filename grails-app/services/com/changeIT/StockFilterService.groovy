package com.changeIT

import grails.gorm.transactions.Transactional

@Transactional
class StockFilterService {

    /** Find all the productos which meet a set of filters.
     * @param filters the fields to search by. Ex.: [dossier.firmDate: new Date(), customer.email: 'email@email.com']
     * @param pageParams the pagination params. Ex.: [max: 10, offset: 0]
     * @param sortParams the sorting params. Ex.: [hqlPath: 'dossier.firmDate', order: 'asc']
     * @param agent the agent in charge of the dossiers
     * @param count if true: get the number of total results, if false: get the results
     * @return the list of resulting dossiers
     */
    def findStock(Map filters, Map pageParams, Map sortParams, Boolean count = false) {
        StringBuilder hqlQueryBuilder = new StringBuilder('SELECT ')
        hqlQueryBuilder << (count ? 'count(stock) ' : 'stock ')
        hqlQueryBuilder << 'FROM Stock as stock INNER JOIN stock.product AS product '

        Map namedParams = [:]

        filters.each { String hqlPath, value ->
            String namedParam = hqlPath.replaceAll(/\./, '')

            if (value instanceof String) {
                hqlQueryBuilder << "WHERE lower(${hqlPath}) LIKE lower(:${namedParam}) "
                value = "%${value}%"
            } else {
                hqlQueryBuilder << "${hqlPath} = :${namedParam} "
            }
            namedParams[namedParam] = value
        }
        if (sortParams && !count) {
            hqlQueryBuilder << "ORDER BY ${sortParams.hqlPath} ${sortParams.order}"
        }

        Stock.executeQuery(hqlQueryBuilder.toString(),namedParams,pageParams)
    }

    /** Find all the productos which meet a set of filters.
     * @param filters the fields to search by. Ex.: [dossier.firmDate: new Date(), customer.email: 'email@email.com']
     * @param pageParams the pagination params. Ex.: [max: 10, offset: 0]
     * @param sortParams the sorting params. Ex.: [hqlPath: 'dossier.firmDate', order: 'asc']
     * @param agent the agent in charge of the dossiers
     * @param count if true: get the number of total results, if false: get the results
     * @return the list of resulting dossiers
     */
    def findStockbyType(String tipo, Map pageParams) {
        StringBuilder hqlQueryBuilder = new StringBuilder('SELECT stock ')
        Map namedParams = [:]

        namedParams[tipo] = "%${tipo}%"


         hqlQueryBuilder << 'FROM Stock as stock INNER JOIN stock.product AS product '
         hqlQueryBuilder << "WHERE lower(tipo) LIKE lower(:${tipo}) "


        Stock.executeQuery(hqlQueryBuilder.toString(),namedParams,pageParams)
    }



}
