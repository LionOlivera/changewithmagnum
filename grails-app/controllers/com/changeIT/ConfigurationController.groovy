package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ConfigurationController {

    ConfigurationService configurationService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond configurationService.list(params), model:[configurationCount: configurationService.count()]
    }
    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        respond configurationService.get(id)
    }
    @Secured(['ROLE_ADMIN'])
    def create() {
        respond new Configuration(params)
    }
    @Secured(['ROLE_ADMIN'])
    def save(Configuration configuration) {
        if (configuration == null) {
            notFound()
            return
        }

        try {
            configurationService.save(configuration)
        } catch (ValidationException e) {
            respond configuration.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'configuration.label', default: 'Configuration'), configuration.id])
                redirect configuration
            }
            '*' { respond configuration, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        respond configurationService.get(id)
    }
    @Secured(['ROLE_ADMIN'])
    def update(Configuration configuration) {
        if (configuration == null) {
            notFound()
            return
        }

        try {
            configurationService.save(configuration)
        } catch (ValidationException e) {
            respond configuration.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'configuration.label', default: 'Configuration'), configuration.id])
                redirect configuration
            }
            '*'{ respond configuration, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        configurationService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'configuration.label', default: 'Configuration'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    @Secured(['ROLE_ADMIN'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'configuration.label', default: 'Configuration'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
