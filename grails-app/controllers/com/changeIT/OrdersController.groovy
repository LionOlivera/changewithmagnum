package com.changeIT

import com.changeIT.util.Base64URL
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.json.JsonSlurper
import grails.plugin.springsecurity.SpringSecurityService
import com.changeIT.util.Util


import static org.springframework.http.HttpStatus.*

class OrdersController {
    OrdersService ordersService
    StockService stockService
    StateService stateService
    StockFilterService stockFilterService
    SpringSecurityService springSecurityService
    CorreoUtil correo = new CorreoUtil()
    Base64URL base64URL = new Base64URL()
    OrdersFilterService ordersFilterService
    SecUserCustomService secUserCustomService

    Util util = new Util()
    public static final String MAIL_TEMPLATES = "mail"
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def roles = springSecurityService.getPrincipal().getAuthorities().toString()
        List<Orders> listOrders
        if (roles == '[ROLE_CLIENTE]')
            listOrders = Orders.findAllBySecUser(SecUser.get(springSecurityService.principal.id))
        else
            listOrders = ordersService.list()
        respond listOrders, model: [ordersList: listOrders, ordersCount: listOrders.size(), total: getTotalAmount(listOrders), offset: 0, max: params.max, orderBy: 'orders.created', orderMode: 'desc']
    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def cleanOrderPlace() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        orderToBuy.clear()
        flash.message = message(code: 'default.updated.placeorder')
        session.setAttribute('orderToBuy', orderToBuy)
        redirect(controller: 'orders', action: 'indexGeneral')
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def cleanPRD() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        int y = 0;
        def stockToDelete = orderToBuy
        for (Orders orders : stockToDelete) {
            if (orders.getProduct().getId() == Integer.valueOf(params.id)) {
                orderToBuy.remove(y)
                break
            }
            y++;
        }
        session.setAttribute('orderToBuy', orderToBuy)
        flash.message = message(code: 'default.updated.placeorder.remove')
        redirect(controller: 'orders', action: 'checkout')
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def checkout() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        if (orderToBuy.size() > 0) {
            def orderInstanceList = orderToBuy
            def cod = util.codPedido()
            for (Orders order : orderInstanceList) {
                order.setCodigoPedido(cod)
            }
            def text = ''
            if (orderToBuy.size() == 0) {
                text = 'Carrito Vacio'
            } else {
                text = 'Productos Selecccionados'
            }

            respond orderInstanceList, model: [orderInstanceList: orderInstanceList, total: getTotalAmount(orderInstanceList), elementos: orderToBuy.size(), texto: text]

        } else {
            flash.message = message(code: 'default.vacio.placeorder')
            redirect(controller: 'orders', action: 'indexGeneral')
        }
    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def getProductbyTipo(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<Stock> productList = stockFilterService.findStockbyType(params.id, [offset: 0, max: params.int('max')])
        render(template: 'generalTable', model: [stockInstanceList: productList, stockInstanceTotal: productList.size()])
    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def indexGeneral(Integer max) {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        params.max = Math.min(max ?: 10, 100)
        Long totalStock = stockFilterService.findStock([:], [:], [:], true).get(0)

        respond stockService.list(params), model: [stockCount: stockService.count(), elementos: orderToBuy.size(), texto: '', stockInstanceTotal: totalStock, stockInstanceList: stockService.list(params), offset: 0, max: params.max, orderBy: 'product.articulo', orderMode: 'DESC']
    }

    def getTotalAmount(List<Orders> listOrders) {
        double total
        for (Orders order : listOrders) {
            total = order.getMontoTotal() + total
        }
        return total.round(2)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def indexCustomer(int id) {
        Customer customer = Customer.findById(id)
        respond Orders.findAllByCustomer(customer), model: [ordersCount: Orders.findAllByCustomer(customer).size()]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def show(Long id) {
        respond ordersService.get(id), model: [stateIntance: stateService.list()]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_CLIENTE'])
    def create() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        Util util = new Util()
        def stockMap = [:]
        def allStock = []

        Stock.list().each { it -> allStock << it }
        for (int i = 0; i < allStock.size(); i++) {
            stockMap.put(allStock.get(i), true)
        }
        orderToBuy.clear()
        session.setAttribute('orderToBuy', orderToBuy)
        params.max = 10
        SecUser agent = SecUser.get(springSecurityService.principal.id)
        Long totalStock = stockFilterService.findStock([:], [:], [:], agent, true).get(0)
        List<Stock> stockLista = stockService.list(params)

        respond stockLista, model: [codPedido: util.codPedido(), stockInstanceList: stockLista, stockInstanceTotal: totalStock, agent: agent, offset: 0, max: params.max, orderBy: 'product.articulo', orderMode: 'desc']

    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def filterListRemote() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        //orderToBuy << new Stock()
        List<Stock> stockLista = Stock.list()
        Orders order = new Orders()
        int i = 0
        for (Stock stock : stockLista) {
            if (String.valueOf(stock.getId()).equalsIgnoreCase(params.stockId)) {
                if (java.lang.Boolean.valueOf(params.flag)) {
                    order.setCantidad(Integer.valueOf(String.valueOf(params.cantidadSeleccionada)))
                    order.setProduct(stock.getProduct())
                    order.setCodigoPedido(String.valueOf(params.codigoPedido))
                    orderToBuy << order
                } else {
                    int y = 0;
                    def stockToDelete = orderToBuy
                    for (Stock producto : stockToDelete.stock) {
                        if (producto.getId() == stock.id) {
                            orderToBuy.remove(y)
                            break
                        }
                        y++;
                    }
                }
            }
            i++;
        }
        session.setAttribute('orderToBuy', orderToBuy)
        render(template: 'resultsTable', controller: 'orders', model: [orderInstanceList: orderToBuy])

    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def filterListRemoteIndexGeneralPRD() {
        //TODO Verificación de filtros (ej.: fechas con formato correspondiente, números en lugar de texto, etc.). Preferiblemente en cliente
        Map filters = new JsonSlurper().parseText(params.filters)
        filters = filters.collectEntries { String hqlPath, value ->
            if (hqlPath.toLowerCase().contains('date')) {
                [(hqlPath): Date.parse('dd/MM/yyyy', value)]
            } else {
                [(hqlPath): value]
            }
        }

        Long totalProduct = stockFilterService.findStock(filters, [:], [:], true).get(0)
        List<Stock> productList = stockFilterService.findStock(filters, [offset: params.int('offset'), max: params.int('max')], [hqlPath: params.orderBy, order: params.orderMode])
        render(template: 'generalTable', model: [stockInstanceList: productList, stockInstanceTotal: totalProduct, offset: params.offset, max: params.max, orderBy: params.orderBy, orderMode: params.orderMode])
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def filterListRemoteIndexGeneral() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        List<Stock> stockLista = Stock.list()
        Orders order = new Orders()
        int i = 0
        for (Stock stock : stockLista) {
            if (String.valueOf(stock.getId()).equalsIgnoreCase(params.stockId)) {
                if (Integer.valueOf(params.cantidadSeleccionada) > 0) {
                    order.setCantidad(Integer.valueOf(String.valueOf(params.cantidadSeleccionada)))
                    order.setProduct(stock.getProduct())
                    order.setCodigoPedido(String.valueOf(params.codigoPedido))
                    order.montoTotal = order.getCantidad() * stock.getProduct().getPrecio()
                    orderToBuy << order
                } else {
                    int y = 0;
                    def stockToDelete = orderToBuy
                    for (Stock producto : stockToDelete.stock) {
                        if (producto.getId() == stock.id) {
                            orderToBuy.remove(y)
                            break
                        }
                        y++
                    }
                }
            }
            i++
        }
        session.setAttribute('orderToBuy', orderToBuy)
        def text = ''
        if (orderToBuy.size() == 0) {
            text = 'Carrito Vacio'
        } else {
            text = 'Productos Selecccionados'
        }
        render(template: '/layouts/nav', model: [orderInstanceList: orderToBuy, elementos: orderToBuy.size(), texto: text])

    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def filterListRemoteStock() {

        SecUser agent = SecUser.get(springSecurityService.principal.id)
        //TODO Verificación de filtros (ej.: fechas con formato correspondiente, números en lugar de texto, etc.). Preferiblemente en cliente
        Map filters = new JsonSlurper().parseText(params.filters)
        filters = filters.collectEntries { String hqlPath, value ->
            if (hqlPath.toLowerCase().contains('date')) {
                [(hqlPath): Date.parse('dd/MM/yyyy', value)]
            } else {
                [(hqlPath): value]
            }
        }
        Long totalStock = stockFilterService.find(filters, [:], [:], agent, true).get(0)
        List<Stock> stockList = stockFilterService.findStock(filters, [offset: params.int('offset'), max: params.int('max')], [hqlPath: params.orderBy, order: params.orderMode], agent)
        render(template: 'resultsTableStockFilter', model: [stockInstanceList: stockList, stockInstanceTotal: totalStock, offset: params.offset, max: params.max, orderBy: params.orderBy, orderMode: params.orderMode])
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def filterListRemoteOrders() {
        SecUser agent = SecUser.get(springSecurityService.principal.id)
        //TODO Verificación de filtros (ej.: fechas con formato correspondiente, números en lugar de texto, etc.). Preferiblemente en cliente
        Map filters = new JsonSlurper().parseText(params.filters)
        filters = filters.collectEntries { String hqlPath, value ->
            if (hqlPath.toLowerCase().contains('created1')) {
                [(hqlPath): Date.parse('dd/MM/yyyy', value)]
            } else {
                [(hqlPath): value]
            }
        }
        Long totalStock = ordersFilterService.findOrder(filters, [:], [:], agent, true).get(0)
        List<Stock> stockList = ordersFilterService.findOrder(filters, [offset: params.int('offset'), max: params.int('max')], [hqlPath: params.orderBy, order: params.orderMode], agent)
        render(template: 'resultsTableOrdersFilter', model: [ordersList: stockList, ordersCount: totalStock, total: getTotalAmount(stockList), stockListoffset: params.offset, max: params.max, orderBy: params.orderBy, orderMode: params.orderMode])
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def guestUser() {


    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def saveGuestOrder() {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        List<ResumenPedido> resumenPedidosList = new ArrayList<ResumenPedido>()
        double montoTotal = 0
        StringBuilder sb = new StringBuilder()
        Customer customer
        sb.append('<ol>')
        try {
            log.info("Vamos a armar el pedidio")
            for (Orders order : orderToBuy) {
                ResumenPedido resume = new ResumenPedido()
                // crear los customer con roles. para poder probar.
                Stock stock = Stock.findByProduct(order.getProduct())
                montoTotal = montoTotal + (order.getProduct().getPrecio() * order.getCantidad())

                if (stock.getCantidad() - order.getCantidad() < 0)
                    stock.setCantidad(0)
                else
                    stock.setCantidad(stock.getCantidad() - order.getCantidad())

                stockService.save(stock)
                resume.setProductoOrdenado(order.getProduct())
                resume.setCantidad(order.getCantidad())
                resumenPedidosList.add(resume)
                sb.append("<li> Producto :  ${order.getProduct().getArticulo()}  Cantidad: ${order.getCantidad()}</li>")
            }
            log.info("Pedido Termiando de armar")
            sb.append("</ol>")
            State state = State.findByCode('1')
            if (Customer.findByEmail(params.email) == null) {
                try {
                    SecUser user
                    if (SecUser.findByEmail(params.email)) {
                        user = SecUser.findByEmail(params.email)
                    } else {
                        user = new SecUser(username: params.email, password: 'Secret0!', enabled: true, accountExpired: false, accountLocked: false, passwordExpired: false, name: 'update', lastName: 'update', phone: params.phone, email: params.email, attempts: 0)
                        secUserCustomService.save(user, SecRole.findByAuthority("ROLE_USER"))
                    }
                    customer = new Customer(secUser: user, email: params.email, phone: params.phone, address: params.address, razonSocial: 'update', name: user.getName(), lastName: user.getLastName(), cuit: 'update', limiteCompra: '1500000', gender: 'update')
                    customer = secUserCustomService.saveUserCustomerGuest(customer, SecRole.findByAuthority("ROLE_CLIENTE"), user)
                    log.info("customer creado con ID: ${customer.getId()}")
                } catch (ValidationException e) {
                    log.error("El pedido no ha podido ser procesado", e)
                    respond orders.errors, view: 'create'
                    return
                }
            } else {
                customer = Customer.findByEmail(params.email)
            }
//validar  si la lista ordertobuy esta vacio
            log.info("Se armo la orden")
            Orders orderPersist = new Orders(secUser: customer.getSecUser(), state: state, montoTotal: montoTotal, customer: customer, resumenPedidos: resumenPedidosList, product: new Product(), cantidad: 0, codigoPedido: orderToBuy.get(0).codigoPedido)

            if (ordersService.save(orderPersist)) {
                log.info("orden grabada, se envia el email con pedido")
                String text = util.getFile(MAIL_TEMPLATES + File.separator + "pedidoTerminado.html")
                text = text.replace("[title]", g.message(code: "mail.tarificador.title", args: [customer.getName() + " " + customer.getLastName()]))
                text = text.replace("[text]", g.message(code: "mail.customer.order.text"))
                text = text.replace("[text2]", sb.toString())
                text = text.replace("[url]", "<a href=\"${g.createLink(controller: "login", action: "auth", absolute: true)}\" target=\"_blank\" style='color: #FFFFFF !important;'>" + g.message(code: "mail.btn.personal.space") + "</a>")
                text = text.replace("[greeting]", g.message(code: "mail.greeting.customer"))
                text = text.replace("[text3]", g.message(code: "mail.customer.order.usuario") + customer.getEmail() + "   " + g.message(code: "mail.customer.order.password") + 'Secret0!')
                correo.sendMail(text, g.message(code: "mail.customer.order.subject.1"), customer.getEmail())
                orderToBuy.clear()
                session.setAttribute('orderToBuy', orderToBuy)
                flash.message = message(code: 'default.order.guest.created')
                redirect(controller: 'orders', action: 'indexGeneral')
            } else {
                flash.message = message(code: 'default.order.guest.error')

            }
        } catch (ValidationException e) {
            respond e, view: 'create'
            return
        }


    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def save(Orders orders) {
        def orderToBuy = []
        if (session.getAttribute('orderToBuy'))
            orderToBuy = session.getAttribute('orderToBuy')
        if (orders == null) {
            notFound()
            return
        }
        SecUser user = SecUser.get(springSecurityService.principal.id)
        Customer customer = Customer.findByEmail(user.email)
        List<ResumenPedido> resumenPedidosList = new ArrayList<ResumenPedido>()
        double montoTotal = 0
        StringBuilder sb = new StringBuilder()

        sb.append('<ol>')
        try {

            for (Orders order : orderToBuy) {
                ResumenPedido resume = new ResumenPedido()
                // crear los customer con roles. para poder probar.
                Stock stock = Stock.findByProduct(order.getProduct())
                montoTotal = montoTotal + (order.getProduct().getPrecio() * order.getCantidad())

                if (stock.getCantidad() - order.getCantidad() < 0)
                    stock.setCantidad(0)
                else
                    stock.setCantidad(stock.getCantidad() - order.getCantidad())
                stockService.save(stock)
                resume.setProductoOrdenado(order.getProduct())
                resume.setCantidad(order.getCantidad())
                resumenPedidosList.add(resume)
                sb.append("<li> Producto :  ${order.getProduct().getArticulo()}  Cantidad: ${order.getCantidad()}</li>")
            }
            sb.append("</ol>")
            State state = State.findByCode('1')
            Orders orderPersist = new Orders(secUser: user, state: state, montoTotal: montoTotal, customer: customer, resumenPedidos: resumenPedidosList, product: new Product(), cantidad: 0, codigoPedido: orderToBuy.get(0).codigoPedido)
            ordersService.save(orderPersist)
            String text = util.getFile(MAIL_TEMPLATES + File.separator + "pedidoTerminado.html")
            text = text.replace("[title]", g.message(code: "mail.tarificador.title", args: [user.getName() + " " + user.getLastName()]))
            text = text.replace("[text]", g.message(code: "mail.customer.order.text"))
            text = text.replace("[text2]", sb.toString())
            text = text.replace("[url]", "<a href=\"${g.createLink(controller: "login", action: "auth")}\" target=\"_blank\" style='color: #FFFFFF !important;'>" + g.message(code: "mail.btn.personal.space") + "</a>")
            text = text.replace("[greeting]", g.message(code: "mail.greeting.customer"))
            correo.sendMail(text, g.message(code: "mail.customer.order.subject.1"), user.getEmail())
            orderToBuy.clear()
            session.setAttribute('orderToBuy', orderToBuy)
            List<Orders> listOrders = Orders.findAllBySecUser(SecUser.get(springSecurityService.principal.id))
            render(view: "index", model: [ordersList: listOrders, ordersCount: listOrders.size(), total: getTotalAmount(listOrders)])
        } catch (ValidationException e) {
            respond orders.errors, view: 'create'
            return
        }

    }

    @Secured(['ROLE_ADMIN', 'ROLE_CLIENTE', 'ROLE_VENDEDOR'])
    def edit(Long id) {
        respond ordersService.get(id), model: [stateIntance: stateService.list()]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def update(Orders orders) {
        if (orders == null) {
            notFound()
            return
        }
        List<State> stateList = State.list()
        def estadoSeleccionado = params.estadoID
        for (State estado : stateList) {
            if (estado.getId().toString().equalsIgnoreCase(estadoSeleccionado)) {
                orders.state = estado
            }
        }

        try {
            ordersService.save(orders)
        } catch (ValidationException e) {
            respond orders.errors, view: 'edit'
            return
        }

        String text = util.getFile(MAIL_TEMPLATES + File.separator + "pedidoActualizado.html")
        text = text.replace("[title]", g.message(code: "mail.tarificador.title", args: [orders.getCustomer().getName() + " " + orders.getCustomer().getName()]))
        text = text.replace("[text]", g.message(code: "mail.customer.order.text.update"))
        text = text.replace("[text2]", orders.state.getDescription())
        text = text.replace("[url]", "<a href=\"${g.createLink(controller: "login", action: "auth")}\" target=\"_blank\" style='color: #FFFFFF !important;'>" + g.message(code: "mail.btn.personal.space") + "</a>")
        text = text.replace("[greeting]", g.message(code: "mail.greeting.customer"))
        correo.sendMail(text, g.message(code: "mail.customer.order.subject.update"), orders.getCustomer().email)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'orders.label', default: 'Orders'), orders.id])
                redirect orders
            }
            '*' { respond orders, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        Orders order = Orders.findById(id)

        List<ResumenPedido> listResumen = order.resumenPedidos
        for (ResumenPedido resumen : listResumen) {

            Stock stock = Stock.findByProduct(resumen.getProductoOrdenado())
            stock.cantidad = stock.cantidad + resumen.cantidad
            stockService.save(stock)

        }
        ordersService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'orders.label', default: 'Orders'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'orders.label', default: 'Orders'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
