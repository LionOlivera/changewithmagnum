package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.json.JsonSlurper

import static org.springframework.http.HttpStatus.*

class StockController {
    def springSecurityService
    StockService stockService
    StockFilterService stockFilterService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        Long totalStock = stockFilterService.findStock([:], [:], [:], true).get(0)
        respond stockService.list(params), model: [stockCount: stockService.count(), stockInstanceTotal: totalStock, stockInstanceList: stockService.list(params), offset: 0, max: params.max, orderBy: 'product.articulo', orderMode: 'desc']

    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def show(Long id) {
        respond stockService.get(id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def create() {
        respond new Stock(params)

    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def filterListRemote() {
        SecUser agent = SecUser.get(springSecurityService.principal.id)

        //TODO Verificación de filtros (ej.: fechas con formato correspondiente, números en lugar de texto, etc.). Preferiblemente en cliente
        Map filters = new JsonSlurper().parseText(params.filters)
        filters = filters.collectEntries { String hqlPath, value ->
            if (hqlPath.toLowerCase().contains('date')) {
                [(hqlPath): Date.parse('dd/MM/yyyy', value)]
            } else {
                [(hqlPath): value]
            }
        }

        Long totalStock = stockFilterService.findStock(filters, [:], [:], agent, true).get(0)
        List<Stock> stockList = stockFilterService.findStock(filters, [offset: params.int('offset'), max: params.int('max')], [hqlPath: params.orderBy, order: params.orderMode], agent)
        render(template: 'resultsTable', model: [stockInstanceList: stockList, stockInstanceTotal: totalStock, offset: params.offset, max: params.max, orderBy: params.orderBy, orderMode: params.orderMode])
    }


    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def save(Stock subscriber) {
        if (subscriber == null) {
            notFound()
            return
        }

        try {
            stockService.save(subscriber)
        } catch (ValidationException e) {
            respond subscriber.errors, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'stock.label', default: 'Product'), subscriber.id])
                redirect subscriber
            }
            '*' { respond subscriber, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def edit(Long id) {
        respond stockService.get(id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def update(Stock subscriber) {
        if (subscriber == null) {
            notFound()
            return
        }

        try {
            stockService.save(subscriber)
        } catch (ValidationException e) {
            respond subscriber.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'stock.label', default: 'Product'), subscriber.id])
                redirect subscriber
            }
            '*' { respond subscriber, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        stockService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'stock.label', default: 'Product'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'stock.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
