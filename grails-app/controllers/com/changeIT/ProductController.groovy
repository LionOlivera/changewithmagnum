package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.json.JsonSlurper

import static org.springframework.http.HttpStatus.*

class ProductController {

    ProductService productService
    def springSecurityService
    ProductFilterService productFilterService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        SecUser agent = SecUser.get(springSecurityService.principal.id)
        Long totalStock = productFilterService.findProduct([:], [:], [:], agent, true).get(0)
        respond productService.list(params), model: [productCount: productService.count(), productInstanceTotal: totalStock, productList: productService.list(params), agent: agent, offset: 0, max: params.max, orderBy: 'product.articulo', orderMode: 'desc']
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def editFeaturedImage(Long id) {
       Product product = Product.findById(id)
        if (!product) {
            notFound()
        }
        [product: product]
    }
    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def featuredImage(Long id) {
        Product product = productService.get(id)
        if (!product || product.featuredImageBytes == null) {
            notFound()
            return
        }
        render file: product.featuredImageBytes,
                contentType: product.featuredImageContentType
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def uploadFeaturedImage(FeaturedImageCommand cmd) {
        if (cmd == null) {
            notFound()
            return
        }

        if (cmd.hasErrors()) {
            respond(cmd.errors, model: [restaurant: cmd], view: 'editFeaturedImage')
            return
        }

        Product product = productService.update(cmd.id,
                cmd.version,
                cmd.featuredImageFile.bytes,
                cmd.featuredImageFile.contentType)

        if (product == null) {
            notFound()
            return
        }

        if (product.hasErrors()) {
            respond(product.errors, model: [product: product], view: 'editFeaturedImage')
            return
        }

        Locale locale = request.locale

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), cmd.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }


    }




    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(Long id) {
        respond productService.get(id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def create() {
        respond new Product(params)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def save(Product product) {
        if (product == null) {
            notFound()
            return
        }

        try {
            productService.save(product)
        } catch (ValidationException e) {
            respond product.errors, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def edit(Long id) {
        respond productService.get(id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def update(Product product) {
        if (product == null) {
            notFound()
            return
        }

        try {
            productService.save(product)
        } catch (ValidationException e) {
            respond product.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        productService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'product.label', default: 'Product'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR'])
    def filterListRemote() {
        SecUser agent = SecUser.get(springSecurityService.principal.id)

        //TODO Verificación de filtros (ej.: fechas con formato correspondiente, números en lugar de texto, etc.). Preferiblemente en cliente
        Map filters = new JsonSlurper().parseText(params.filters)
        filters = filters.collectEntries { String hqlPath, value ->
            if (hqlPath.toLowerCase().contains('date')) {
                [(hqlPath): Date.parse('dd/MM/yyyy', value)]
            } else {
                [(hqlPath): value]
            }
        }

        Long totalProduct = productFilterService.findProduct(filters, [:], [:], agent, true).get(0)
        List<Product> productList = productFilterService.findProduct(filters, [offset: params.int('offset'), max: params.int('max')], [hqlPath: params.orderBy, order: params.orderMode], agent)
        render(template: 'resultsTable', model: [productList: productList, productInstanceTotal: totalProduct, offset: params.offset, max: params.max, orderBy: params.orderBy, orderMode: params.orderMode])
    }



    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
