package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SecRoleController {

    SecRoleService secRoleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond secRoleService.list(params), model:[secRoleCount: secRoleService.count()]
    }
    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        respond secRoleService.get(id)
    }
    @Secured(['ROLE_ADMIN'])
    def create() {
        respond new SecRole(params)
    }
    @Secured(['ROLE_ADMIN'])
    def save(SecRole secRole) {
        if (secRole == null) {
            notFound()
            return
        }

        try {
            secRoleService.save(secRole)
        } catch (ValidationException e) {
            respond secRole.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'secRole.label', default: 'SecRole'), secRole.id])
                redirect secRole
            }
            '*' { respond secRole, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        respond secRoleService.get(id)
    }
    @Secured(['ROLE_ADMIN'])
    def update(SecRole secRole) {
        if (secRole == null) {
            notFound()
            return
        }

        try {
            secRoleService.save(secRole)
        } catch (ValidationException e) {
            respond secRole.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'secRole.label', default: 'SecRole'), secRole.id])
                redirect secRole
            }
            '*'{ respond secRole, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        secRoleService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'secRole.label', default: 'SecRole'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    @Secured(['ROLE_ADMIN'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secRole.label', default: 'SecRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
