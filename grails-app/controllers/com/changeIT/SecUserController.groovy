package com.changeIT

import com.changeIT.util.Util
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.plugin.htmlcleaner.HtmlCleaner

class SecUserController {
    HtmlCleaner htmlCleaner
    SecUserService secUserService
    SecUserSecRoleService secUserSecRoleService
    SecUserCustomService secUserCustomService
    CorreoUtil correo = new CorreoUtil()
    Util util = new Util()

    static allowedMethods = [index : ['GET'], create: ['GET'], show: ['GET'], edit: ['GET'],
                             update: ['POST'], save: ['POST'], delete: ['POST']]

    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond secUserService.list(params), model: [secUserCount: secUserService.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        SecUser secUserInstance = SecUser.get(id)
        if (!secUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'secUser.label', default: 'SecUser'), id])
            redirect(action: "list")
            return
        }

        SecUserSecRole secUserSecRole = SecUserSecRole.findBySecUser(secUserInstance)

        [secUserInstance: secUserInstance, role: secUserSecRole.secRole]
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        [secUserInstance: chainModel?.secUserInstance ?: new SecUser(params), rolesInstance: SecRole.list()]
    }

    @Secured(['ROLE_ADMIN'])
    def save(SecUser secUser) {
        if (secUser == null) {
            notFound()
            return
        }

        try {
            SecUser user = new SecUser(username: htmlCleaner.cleanHtml(params.username, 'none'), password: params.password, enabled: params.enabled != null ? true : false, accountExpired: params.accountExpired != null ? true : false, accountLocked: params.accountLocked != null ? true : false, passwordExpired: params.passwordExpired != null ? true : false, name:  htmlCleaner.cleanHtml(params.name, 'none'), lastName: htmlCleaner.cleanHtml(params.lastName, 'none'), phone: htmlCleaner.cleanHtml(params.phone, 'none'), email: htmlCleaner.cleanHtml(params.email, 'none'),attempts: 0)

            secUserService.save(user)
            SecRole secRole = SecRole.get(params.roles)
            SecUserSecRole userRole = new SecUserSecRole(secUser: user, secRole: secRole)
            secUserSecRoleService.save(userRole)

            String MAIL_TEMPLATES = "mail"
            String text = util.getFile(MAIL_TEMPLATES + File.separator + "userPassInform.html")
            text = text.replace("[password]", htmlCleaner.cleanHtml(params.password, 'none'))
            text = text.replace("[email]", user.getEmail())
            text = text.replace("[title]", g.message(code: "mail.tarificador.title"))
            text = text.replace("[text]", g.message(code: "mail.user.create.text"))
            text = text.replace("[text2]", g.message(code: "mail.user.create.text2"))
            text = text.replace("[text3]", g.message(code: "mail.user.create.text3"))
            text = text.replace("[text4]", g.message(code: "mail.user.create.text4"))
            text = text.replace("[greeting]", g.message(code: "mail.greeting"))
            correo.sendMail(text, g.message(code: "mail.user.subject.update"), user.getEmail())

            redirect(action: "show", id: user.id)
        } catch (ValidationException e) {
            respond secUser.errors, view: 'create'
            return
        }


    }

    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def secUserInstance = SecUser.get(id)

        try {

            if (!secUserInstance) {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secUser.label', default: 'SecUser'), id])
                redirect(action: "list")
                return
            }

            def roleMap = [:]
            def allRoles = []
            def roles = SecRole.list()
            def userRoles = SecUserSecRole.findBySecUser(secUserInstance)
            def selectedRoles = []

            roles.each { it ->
                allRoles << it
            }

            userRoles.each { it ->
                selectedRoles << it.secRole
            }

            for (int i = 0; i < allRoles.size(); i++) {

                if (secUserCustomService.isSelected(allRoles.get(i), selectedRoles)) {
                    roleMap.put(allRoles.get(i), true)
                } else {
                    roleMap.put(allRoles.get(i), false)
                }

            }

            [secUserInstance: secUserInstance, roleMap: roleMap]


        } catch (Exception e) {
            redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method edit() from SecUserController", e: e, cont: "secUser", act: "list"])
        }
    }

    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        String MAIL_TEMPLATES = "mail"
        def secUserInstance = SecUser.get(id)
        String text = util.getFile(MAIL_TEMPLATES + File.separator + "userPassInform.html")
        withForm {
            try {
                def username = htmlCleaner.cleanHtml(params.username, 'none')
                def password = params.password
                def enabled = params.enabled != null ? true : false
                def passwordExpired = params.passwordExpired != null ? true : false
                def accountExpired = params.accountExpired != null ? true : false
                def accountLocked = params.accountLocked != null ? true : false
                def selectedRole = params.role
                def email = params.email
                def name = htmlCleaner.cleanHtml(params.name, 'none')
                def lastName = htmlCleaner.cleanHtml(params.lastName, 'none')
                def phone = htmlCleaner.cleanHtml(params.phone, 'none')

                secUserInstance.setUsername(username)

                secUserInstance.setPassword(password)
                secUserInstance.setEnabled(enabled)
                secUserInstance.setPasswordExpired(passwordExpired)
                secUserInstance.setAccountExpired(accountExpired)
                secUserInstance.setAccountLocked(accountLocked)

                secUserInstance.setEmail(email)
                secUserInstance.setName(name)
                secUserInstance.setLastName(lastName)
                secUserInstance.setPhone(phone)
                secUserInstance.setAttempts(0)

                def role = SecRole.get(selectedRole)

                SecUserSecRole.removeAll(secUserInstance)

                if (!secUserInstance.save(flush: true)) {
                    render(view: "edit", model: [secUserInstance: secUserInstance])
                    return
                }

                SecUserSecRole userRole = new SecUserSecRole(secUser: secUserInstance, secRole: role)
                userRole.save(flush: true)

                text = text.replace("[password]", htmlCleaner.cleanHtml(password, 'none'))
                text = text.replace("[email]", email)
                text = text.replace("[title]", g.message(code: "mail.tarificador.title"))
                text = text.replace("[text]", g.message(code: "mail.user.create.text"))
                text = text.replace("[text2]", g.message(code: "mail.user.create.text2"))
                text = text.replace("[text3]", g.message(code: "mail.user.create.text3"))
                text = text.replace("[text4]", g.message(code: "mail.user.create.text4"))
                text = text.replace("[greeting]", g.message(code: "mail.greeting"))
                correo.sendMail(text, g.message(code: "mail.user.subject.update"), email)
                flash.message = message(code: 'default.updated.message', args: [message(code: 'secUser.label', default: 'SecUser'), secUserInstance.id])
                redirect(action: "show", id: secUserInstance.id)
            } catch (Exception e) {
                redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method update() from SecUserController", e: e.getMessage(), cont: "secUser", act: "list"])
            }
        }.invalidToken {
            // bad request
        }


    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        secUserService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'secUser.label', default: 'SecUser'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secUser.label', default: 'SecUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
