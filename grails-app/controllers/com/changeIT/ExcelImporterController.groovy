package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import org.apache.poi.xssf.usermodel.XSSFWorkbook

class ExcelImporterController {
    @Secured(['ROLE_ADMIN'])
    def index() {}

    @Secured(['ROLE_ADMIN'])
    def uploadFile() {
        def file = request.getFile('excelFile')
        if (!file.empty) {
            def sheetheader = []
            def values = []
            def workbook = new XSSFWorkbook(file.getInputStream())
            def sheet = workbook.getSheetAt(0)

            for (cell in sheet.getRow(0).cellIterator()) {
                sheetheader << cell.stringCellValue
            }

            def headerFlag = true
            for (row in sheet.rowIterator()) {
                if (headerFlag) {
                    headerFlag = false
                    continue
                }
                def value = ''
                def map = [:]
                for (cell in row.cellIterator()) {
                    switch (cell.cellType) {
                        case 1:
                            value = cell.stringCellValue
                            map["${sheetheader[cell.columnIndex]}"] = value
                            break
                        case 0:
                            value = cell.numericCellValue
                            map["${sheetheader[cell.columnIndex]}"] = value
                            break
                        default:
                            value = ''
                    }
                }
                values.add(map)
            }

            values.each { v ->
                if (v) {

                    if (Product.findByArticulo(v.articulo) == null) {

                        Product product = new Product(codigo: v.codigo, articulo: v.articulo, precio: v.precio, mdl: v.mdl, obs: v.obs, tipo: v.tipo).save(flush: true, failOnError: true)
                        Stock stock = new Stock(product:product,cantidad: Double.valueOf(String.valueOf(v.cantidad)),obs: v.obs).save(flush: true, failOnError: true)

                    }
                }
            }

            flash.message = "Stock imported successfully"
            redirect action: "index"
        }
    }
}
