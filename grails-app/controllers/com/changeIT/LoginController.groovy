package com.changeIT

import com.changeIT.util.Base64URL
import com.changeIT.util.Util
import com.megatome.grails.RecaptchaService
import grails.plugin.htmlcleaner.HtmlCleaner
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured

import static com.changeIT.util.TimeStamp.compareDates


class LoginController {
    HtmlCleaner htmlCleaner
    def springSecurityService
    RecaptchaService recaptchaService
    SecUserService secUserService
    CorreoUtil correo = new CorreoUtil()
    static allowedMethods = [loggedIn: ['GET']]
    Util util = new Util()
    public static final String MAIL_TEMPLATES = "mail"
    LoginAccessLogService loginAccessLogService



    def auth = {

        System.out.println(params)

        log.debug request.serverName
        log.debug request.serverName.encodeAsMD5()

        def config = SpringSecurityUtils.securityConfig

        if (springSecurityService.isLoggedIn()) {
            redirect uri: config.successHandler.defaultTargetUrl
            return
        } else {
            if (params.login_error.toString().equals("1")) {
                //flash.message = message(code: 'login.auth.badcredentials')
                flash.message = "Ningún usuario coincide con este nombre de usuario y contraseña"
            }
        }

        String view = 'auth'
        String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
        render view: view, model: [postUrl: postUrl, rememberMeParameter: config.rememberMe.parameter]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def loggedIn() {
        try {
            SecUser user = springSecurityService.currentUser

            def roleAdmin = SecRole.findByAuthority("ROLE_ADMIN")
            def roleVendedor = SecRole.findByAuthority("ROLE_VENDEDOR")
            loginAccessLogService.saveFromRequest(request, user)
            if (user.authorities.id.contains(roleAdmin.id) || user.authorities.id.contains(roleVendedor.id)) {
                redirect(controller: 'customer')
                return
            }
            def roleCliente = SecRole.findByAuthority("ROLE_CLIENTE")
            if (user.authorities.id.contains(roleCliente.id)) {
                redirect(controller: 'orders', action:'checkout')
                return
            }


        } catch (Exception e) {
            redirect(controller: "excption", action: "generalException", params: [message: "There was an error while calling method login() from LoginController", e: e.getMessage(), cont: "login", act: "loggedIn("])
        }
    }


    def sendRecoveryEmail(SecUser user) {
        String text = util.getFile(MAIL_TEMPLATES + File.separator + "userRetrivePassword.html")
        def token = Token.findByEmail(user.email)
        if (!token) {
            token = new Token(email: user.getEmail())
            token.save(flush: true)
        }
        def valueEncrypted = util.encrypt("userId=" + user.getId() + "&" + "serverTime=" + System.currentTimeMillis() + "&" + "token=" + token.getValue(), Configuration.findByProperty("keyEncriptor").value)
        Customer customer = Customer.findByEmail(user.getEmail())


        text = text.replace("[title]", g.message(code: "mail.tarificador.title", args: [user.getName() + " " + user.getLastName()]))
        text = text.replace("[text2]", g.message(code: "mail.customer.retrive.text.1"))
        text = text.replace("[url]", "<a href=\"${g.createLink(controller: "registration", action: "retrivePassword", params: [parametros: Base64URL.encodeURL(valueEncrypted)], absolute: true)}\" target=\"_blank\" style='color: #FFFFFF !important;'>" + g.message(code: "mail.btn.personal.space") + "</a>")
        text = text.replace("[greeting]", g.message(code: "mail.greeting"))
        correo.sendMail(text, g.message(code: "mail.customer.retrive.subject.1"), user.getEmail())
        flash.message = message(code: 'login.password.recovery.message.mail.1')
        redirect(controller: "login", action: "auth")
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def retrivePasswordEmail() {
        withForm {
            def recaptchaOK = true
            if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
                recaptchaOK = false
            }
            if (String.valueOf(htmlCleaner.cleanHtml(params.arg0, 'none')).contains("@") && recaptchaOK) {
                SecUser user = new SecUser()
                user = SecUser.findByUsername(params.arg0);
                if (user == null) {
                    user = SecUser.findByEmail(params.arg0);
                    if (user == null) {
                        flash.message = message(code: 'login.password.recovery.message.2')
                        redirect(controller: "login", action: "auth")
                    } else {
                        if (!user.hasErrors())
                            sendRecoveryEmail(user)
                        else {
                            flash.message = message(code: 'login.password.recovery.message.3')
                            redirect(controller: "login", action: "auth")
                        }
                    }
                } else {
                    if (!user.hasErrors())
                        sendRecoveryEmail(user)
                    else {
                        flash.message = message(code: 'login.password.recovery.message.3')
                        redirect(controller: "login", action: "auth")
                    }
                }

            } else {
                if (!recaptchaOK) {
                    flash.message = message(code: 'login.password.recovery.message.1')
                } else {
                    flash.message = message(code: 'login.password.recovery.message.2')
                }
                redirect(controller: "login", action: "auth")
            }
        }
    }


    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def updateUser() {
        withForm {
            try {
                SecUser user = SecUser.findById(session.getAttribute("userId"));
                if (params.j_password.toString().equals(params.j_username.toString())) {
                    user.setPassword(params.j_password.toString())
                    if (secUserService.save(user)) {
                        flash.message = 'Mot de passe mis à jour'
                        redirect(controller: "login", action: "auth")
                    }
                } else {
                    chain(action: 'registration', model: [error: "Les mots de passe doivent être identiques"])
                }
            } catch (Exception e) {
                redirect(controller: "exception", action: "generalException", params: [message: "There was an error while calling method updateUser() from RegistrationController", e: e.getMessage(), cont: "login", act: "auth"])
            }
        }
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def retrivePassword() {
        Map<String, String> map = new HashMap<String, String>();
        def valueDecrypted = util.decrypt(params.parametros, Configuration.findByProperty("keyEncriptor").value)
        String[] params = valueDecrypted.split("&");
        map = util.getParameters(params)
        def idUser = map.get("userId")
        def serverTime = map.get("serverTime")
        String tokenValue = map.get("token")
        if (compareDates(String.valueOf(serverTime))) {
            SecUser user = SecUser.findById(idUser);

            def token = tokenValue ? Token.findByValue(tokenValue) : null
            if (!token || token == null) {
                //No existe el token
                flash.message = 'Este enlace ya ha sido utilizado.'
                redirect(controller: "login", action: "auth")
            } else {
                if (token.getEmail().equalsIgnoreCase(user.getEmail()) || token.getEmail().equalsIgnoreCase(user.username)) {
                    token.delete(flush: true)
                    session.setAttribute("userId", idUser)
                    render(view: 'retrivePassword', model: chainModel ?: [:])
                } else {
                    flash.message = 'Este enlace ya ha sido utilizado.'
                    redirect(controller: "login", action: "auth")
                }
            }
        } else {
            flash.message = 'Este enlace ha caducado.'
            redirect(controller: "login", action: "auth")
        }
    }
}

