package com.changeIT

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ResumenPedidoController {

    ResumenPedidoService resumenPedidoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def index(String id) {
         respond Orders.findByCodigoPedido(id).getResumenPedidos(), model:[resumenPedidoCount: Orders.findByCodigoPedido(id).getResumenPedidos().size()]
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def show(Long id) {
        respond resumenPedidoService.get(id)
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def create() {
        respond new ResumenPedido(params)
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def save(ResumenPedido resumenPedido) {
        if (resumenPedido == null) {
            notFound()
            return
        }

        try {
            resumenPedidoService.save(resumenPedido)
        } catch (ValidationException e) {
            respond resumenPedido.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'resumenPedido.label', default: 'ResumenPedido'), resumenPedido.id])
                redirect resumenPedido
            }
            '*' { respond resumenPedido, [status: CREATED] }
        }
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def edit(Long id) {
        respond resumenPedidoService.get(id)
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def update(ResumenPedido resumenPedido) {
        if (resumenPedido == null) {
            notFound()
            return
        }

        try {
            resumenPedidoService.save(resumenPedido)
        } catch (ValidationException e) {
            respond resumenPedido.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'resumenPedido.label', default: 'ResumenPedido'), resumenPedido.id])
                redirect resumenPedido
            }
            '*'{ respond resumenPedido, [status: OK] }
        }
    }
    @Secured(['ROLE_ADMIN', 'ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        resumenPedidoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'resumenPedido.label', default: 'ResumenPedido'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'resumenPedido.label', default: 'ResumenPedido'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
