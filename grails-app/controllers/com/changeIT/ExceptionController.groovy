package com.changeIT

import grails.plugin.springsecurity.annotation.Secured

class ExceptionController {

    @Secured(['ROLE_USER','ROLE_ADMIN','ROLE_API','ROLE_CLIENTE','ROLE_VENDEDOR','IS_AUTHENTICATED_ANONYMOUSLY'])
    def generalException() {
        log.error(params.message + "." + params.e)
        [message:"Se ha generado una exception",cont:params.cont, act:params.act]
    }
}
