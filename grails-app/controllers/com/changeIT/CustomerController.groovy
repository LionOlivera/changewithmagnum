package com.changeIT

import com.changeIT.util.Base64URL
import com.changeIT.util.Util
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CustomerController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    CustomerService customerService
    SecUserCustomService secUserCustomService
    CorreoUtil correo = new CorreoUtil()
    Base64URL base64URL = new Base64URL()
    Util util = new Util()

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond customerService.list(params), model: [customerCount: customerService.count()]
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR'])
    def index2() {
        params.max = 10
        respond customerService.list(params), model: [customerCount: customerService.count()]
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def show(Long id) {
        respond customerService.get(id)
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR'])
    def create() {
        respond new Customer(params)
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR'])
    def save(Customer customer) {
        String MAIL_TEMPLATES = "mail"
        if (customer == null) {
            notFound()
            return
        }

        try {
            SecUser user = new SecUser(username: customer.email, password: params.passwordID, enabled: true, accountExpired: false, accountLocked: false, passwordExpired: false, name:  customer.getName(), lastName: customer.getLastName(), phone: customer.getPhone(), email: customer.getEmail(),attempts: 0)
            customer.secUser = user
            customer = secUserCustomService.saveUserCustomer(customer,SecRole.findByAuthority("ROLE_CLIENTE"))
        } catch (ValidationException e) {
            respond customer.errors, view: 'create'
            return
        }
        String text = util.getFile(MAIL_TEMPLATES + File.separator + "userPassInform.html")

        text = text.replace("[password]",  params.passwordID)
        text = text.replace("[email]", customer.email)
        text = text.replace("[title]", g.message(code: "mail.tarificador.title"))
        text = text.replace("[text]", g.message(code: "mail.user.create.text"))
        text = text.replace("[text2]", g.message(code: "mail.user.create.text2"))
        text = text.replace("[text3]", g.message(code: "mail.user.create.text3"))
        text = text.replace("[text4]", g.message(code: "mail.user.create.text4"))
        text = text.replace("[greeting]", g.message(code: "mail.greeting"))

        correo.sendMail(text, g.message(code: "mail.user.subject.update"), customer.email)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'customer.label', default: 'Customer'), customer.id])
                redirect customer
            }
            '*' { respond customer, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def edit(Long id) {
        respond customerService.get(id)
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR', 'ROLE_CLIENTE'])
    def update(Customer customer) {
        if (customer == null) {
            notFound()
            return
        }

        try {
            customerService.save(customer)
        } catch (ValidationException e) {
            respond customer.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'customer.label', default: 'Customer'), customer.id])
                redirect customer
            }
            '*' { respond customer, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        customerService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'customer.label', default: 'Customer'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_VENDEDOR'])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
