package com.changeIT

class LoginAccessLog {

    String ipAddress
    String browser
    String operatingSystem

    Date createdDate

    static belongsTo = [user: SecUser]

}
