package com.changeIT

class Product {
    String codigo
    String articulo
    double precio
    String tipo
    String mdl
    String obs
    byte[] featuredImageBytes
    String featuredImageContentType
    static constraints = {
        codigo blank: false, unique: true
        articulo blank: false, unique: true
        codigo codigo: true
        articulo articulo: true
        precio precio: true
        precio blank: false
        codigo(nullable:true)
        articulo(nullable:true)
        precio(nullable:true)
        tipo(nullable:true)
        mdl(nullable:true)
        obs(nullable:true)
        featuredImageBytes nullable: true
        featuredImageContentType nullable: true
    }

    static mapping = {
        featuredImageBytes column: 'featured_image_bytes', sqlType: 'longblob'
    }

}
