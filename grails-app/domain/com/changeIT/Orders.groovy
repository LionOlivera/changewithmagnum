package com.changeIT

class Orders {


    Product product
    int cantidad
    State state
    static belongsTo = [customer: Customer]
    List<ResumenPedido> resumenPedidos
    String codigoPedido
    Date created = new Date()
    double montoTotal

    static constraints = {
        product(nullable:true)
        codigoPedido(nullable:true)
        customer(nullable:true)
    }

    SecUser secUser
}
