package com.changeIT

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic
import groovy.util.logging.Slf4j


@Slf4j
@GrailsCompileStatic
@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class SecUser implements Serializable {
    transient springSecurityService
    private static final long serialVersionUID = 1

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    String name
    String lastName
    String phone
    String email
    Integer attempts

    Set<SecRole> getAuthorities() {
        (SecUserSecRole.findAllBySecUser(this) as List<SecUserSecRole>)*.secRole as Set<SecRole>
    }

    static constraints = {
        username nullable: false, blank: false, unique: true
        password blank: false, validator: { value ->
            Boolean hasMayus = (value =~ /\p{Upper}/).find()
            Boolean hasMinus = (value =~ /\p{Lower}/).find()
            Boolean hasSymbol = (value =~ /\p{Punct}/).find()
            Boolean hasNumber = (value =~ /\d/).find()
            (hasMayus && hasMinus && hasSymbol && hasNumber ) ?: 'format.error'
        }
        password minSize: 8
        name(nullable: true)
        lastName(nullable: true)
        phone(nullable: true)
        email(unique: false)

    }

    static mapping = {
        autowire true
        password column: '`password`'
    }



}
