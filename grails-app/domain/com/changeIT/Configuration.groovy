package com.changeIT

import grails.compiler.GrailsCompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


@GrailsCompileStatic
@ToString( includeNames=true, includePackage=false)
class Configuration implements Serializable  {
	
	String property
	String value

	static Configuration create(String property, String value, boolean flush = false) {
		Configuration instance = new Configuration(property: property, value: value).save(flush: flush)

		instance
	}

	static constraints = {
		property nullable: false, blank: false, unique: true
	}

	static mapping = {
		cache true
	}

}
