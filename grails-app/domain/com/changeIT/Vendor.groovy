package com.changeIT

class Vendor {

    String name
    String lastName
    String address
    String razonSocial
    String cuit
    String email
    String position
    Date created = new Date()

    static constraints = {
        email email: true
    }
}
