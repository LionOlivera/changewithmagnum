package com.changeIT

class Customer {
    enum Gender {
        customerSexMale, customerSexFemale
    }
    String name
    String lastName
    String email
    String address
    String cuit
    String phone
    String razonSocial
    Double limiteCompra
    Date created = new Date()
    String gender
    SecUser secUser
    static hasMany = [orders: Orders]
    static constraints = {
        email email: true
        secUser(nullable:false)
        name(nullable:true)
        lastName(nullable:true)
        address(nullable:true)
        cuit(nullable:true)
        razonSocial(nullable:true)
        limiteCompra(nullable:true)
        gender(nullable:true)
    }

}
